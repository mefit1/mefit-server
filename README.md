﻿# meFIT - API

## Team members

* Antony Charles Allen (mentor)
* Mari Teiler-Johnsen
* Jacob Johnsen
* Ola Matre
* Marius Nygård

## Installation

### Command line

#### Prerequisites

* [.NET Core SDK](https://aka.ms/dotnet-download)
* [SQL Server](https://go.microsoft.com/fwlink/?linkid=866662)

#### Steps

1. Ensure the connection string is set to an avialiable connection, in ```StartUp.cs```
	```csharp
	services.AddDbContext<AppDbContext>(options =>
    {
        options.UseSqlServer(Configuration.GetConnectionString("Default"));
    });
	```
2. Configure the Email-service in ```appsettings.json```
	```json
    "EmailSettings": {
      "SmtpHost": "xxxxxxxxx",
      "SmtpPort": xxxx,
      "SmtpPass": "xxxxxxxxxx",
      "SmtpUser": "xxxxxxxxxx"
    },
	```
3. Open directory ..\meFIT and run **dotnet build**
4. Run **dotnet run --project .\meFIT.BackendAPI\meFIT.BackendAPI.csproj**
5. Open <http://localhost:5000> or <https://localhost:5001>