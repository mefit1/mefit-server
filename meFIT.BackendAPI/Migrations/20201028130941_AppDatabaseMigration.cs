﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace meFIT.BackendAPI.Migrations
{
    public partial class AppDatabaseMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddressLine1 = table.Column<string>(maxLength: 100, nullable: false),
                    AddressLine2 = table.Column<string>(maxLength: 100, nullable: true),
                    AddressLine3 = table.Column<string>(maxLength: 100, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 10, nullable: false),
                    City = table.Column<string>(maxLength: 90, nullable: false),
                    Country = table.Column<string>(maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConcreteName = table.Column<string>(maxLength: 50, nullable: false),
                    GeneralName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Exercise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    Video = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gender",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gender", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GoalProgram",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalProgram", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Program",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Program", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Password = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    ApplyForContributor = table.Column<bool>(nullable: false),
                    Role = table.Column<string>(nullable: true),
                    ProfileId = table.Column<int>(nullable: false),
                    IsConfirmed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Workout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workout", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseCategory",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false),
                    ExerciseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseCategory", x => new { x.ExerciseId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_ExerciseCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExerciseCategory_Exercise_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalProgramCategory",
                columns: table => new
                {
                    GoalProgramId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalProgramCategory", x => new { x.GoalProgramId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_GoalProgramCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalProgramCategory_GoalProgram_GoalProgramId",
                        column: x => x.GoalProgramId,
                        principalTable: "GoalProgram",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProgramCategory",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false),
                    ProgramId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramCategory", x => new { x.ProgramId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_ProgramCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramCategory_Program_ProgramId",
                        column: x => x.ProgramId,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Profile",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: true),
                    Weight = table.Column<float>(nullable: false),
                    Height = table.Column<float>(nullable: false),
                    MedicalConditions = table.Column<string>(maxLength: 255, nullable: true),
                    Disabilities = table.Column<string>(maxLength: 255, nullable: true),
                    GenderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profile", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Profile_Address_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Address",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Profile_Gender_GenderId",
                        column: x => x.GenderId,
                        principalTable: "Gender",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Profile_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalProgramWorkout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    IsCompleted = table.Column<bool>(nullable: false),
                    DaysFromStart = table.Column<int>(nullable: false),
                    GoalProgramId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalProgramWorkout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoalProgramWorkout_GoalProgram_GoalProgramId",
                        column: x => x.GoalProgramId,
                        principalTable: "GoalProgram",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalProgramWorkout_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProgramWorkout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DaysFromStart = table.Column<int>(nullable: false),
                    ProgramId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramWorkout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramWorkout_Program_ProgramId",
                        column: x => x.ProgramId,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramWorkout_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExerciseRepetitions = table.Column<int>(nullable: false),
                    ExerciseId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Set_Exercise_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Set_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkoutCategory",
                columns: table => new
                {
                    CategoryId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkoutCategory", x => new { x.WorkoutId, x.CategoryId });
                    table.ForeignKey(
                        name: "FK_WorkoutCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkoutCategory_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Goal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    GoalProgramId = table.Column<int>(nullable: true),
                    ProfileId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Goal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Goal_GoalProgram_GoalProgramId",
                        column: x => x.GoalProgramId,
                        principalTable: "GoalProgram",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Goal_Profile_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profile",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GoalWorkout",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Owner = table.Column<string>(nullable: true),
                    IsCompleted = table.Column<bool>(nullable: false),
                    DaysFromStart = table.Column<int>(nullable: false),
                    GoalId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GoalWorkout", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GoalWorkout_Goal_GoalId",
                        column: x => x.GoalId,
                        principalTable: "Goal",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GoalWorkout_Workout_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workout",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Address",
                columns: new[] { "Id", "AddressLine1", "AddressLine2", "AddressLine3", "City", "Country", "PostalCode" },
                values: new object[,]
                {
                    { 1, "Drammensveien 1", null, null, "Oslo", "Norway", "0010" },
                    { 2, "1st Avenue 45", null, null, "Leeds, AL", "USA", "35094" },
                    { 3, "Skaugumsåsen 1", null, null, "Asker", "Norway", "1399" },
                    { 4, "Lakkegata 14B", null, null, "Oslo", "Norway", "0556" },
                    { 5, "Gran Vía 100", null, null, "Madrid", "Spain", "23487" },
                    { 6, "Granskauen 1050F", null, null, "Hønefoss", "Norway", "4510" }
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "Id", "ConcreteName", "GeneralName" },
                values: new object[,]
                {
                    { 16, "Forearms", "Arms" },
                    { 15, "Hamstring", "Legs" },
                    { 14, "Upper back", "Torso" },
                    { 12, "Sideabs", "Torso" },
                    { 11, "Front legs", "Legs" },
                    { 10, "Triceps", "Arms" },
                    { 9, "Biceps", "Arms" },
                    { 13, "Quadriceps", "Legs" },
                    { 7, "Shoulders", "Arms" },
                    { 6, "Glutes", "Legs" },
                    { 5, "Calfs", "Legs" },
                    { 4, "Legs", "Legs" },
                    { 3, "Middle back", "Torso" },
                    { 2, "Chest", "Torso" },
                    { 1, "Abs", "Torso" },
                    { 8, "Lower back", "Torso" }
                });

            migrationBuilder.InsertData(
                table: "Exercise",
                columns: new[] { "Id", "Description", "Image", "Name", "Owner", "Video" },
                values: new object[,]
                {
                    { 1, "Skip on one foot for 10 seconds, than repeat on the other foot.", "", "Skips", "harald.rex@kongehuset.no", "" },
                    { 2, "Lay on your back, bring your arms behind your head and lift your torso before you slowly lower it again.", "", "Sit-Ups", "harald.rex@kongehuset.no", "" },
                    { 3, "Lay faced down, bring your hands to the floor on both sides next to your chest, lift your body so only your hands and toes touch the floor. When your arms are straight, lower your body steady to the floor again.", "", "Push-Ups", "harald.rex@kongehuset.no", "" }
                });

            migrationBuilder.InsertData(
                table: "Gender",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 3, "Other" },
                    { 1, "Male" },
                    { 2, "Female" }
                });

            migrationBuilder.InsertData(
                table: "GoalProgram",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Basic Core Program" });

            migrationBuilder.InsertData(
                table: "Program",
                columns: new[] { "Id", "Name", "Owner" },
                values: new object[] { 1, "Basic Core Program", "harald.rex@kongehuset.no" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "ApplyForContributor", "DOB", "Email", "FirstName", "IsConfirmed", "LastName", "Password", "ProfileId", "Role" },
                values: new object[,]
                {
                    { 6, false, new DateTime(1955, 9, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "kari.nordmann@ciber.no", "Kari", true, "Nordmann", "AQAAAAEAACcQAAAAEBkE1GCI/JTgI+l3ioqn79KYEO8t1yxpYSfPYVyGJt7GVBJ5ljqyIdSDMf/5ZpIPSg==", 0, "Contributer, Admin" },
                    { 1, false, new DateTime(1937, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "harald.rex@kongehuset.no", "Harald", true, "Rex", "AQAAAAEAACcQAAAAEN5yj0dVafakUOAU4dStowrrP1CdtJWnFvr+vVCHvuGzlrRUQG46dgUbTTYxbAFMHw==", 0, "Admin" },
                    { 2, false, new DateTime(1963, 2, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "charles.barkley@nba.com", "Charles", true, "Barkley", "AQAAAAEAACcQAAAAEJWZZmQ/wDO83jo7Ga2sdQ+JG2NmR4qOhKaly/pi9A6ZfeqlV2U4zx369MxKDWo6uA==", 0, "Contributer, Admin" },
                    { 3, false, new DateTime(1973, 7, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "haakon.magnus@kongehuset.no", "Haakon Magnus", true, "Crown Prince of Norway", "AQAAAAEAACcQAAAAEABlI+5l6Qk17rIgGDH19zdAbXhiUA4n0F6mdYuksUwwKVywDRRyIAhvkXYE9QXS7A==", 0, "User, Contributer, Admin" },
                    { 4, false, new DateTime(1962, 1, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "j_johansen@hotmail.com", "Jan", true, "Johansen", "AQAAAAEAACcQAAAAEPb5MEK96PN5tE4FqUulqyLGwy8UN3Jz+zYs5BK7wRulDljqhDQsXvlb/UAxGEYoig==", 0, "User, Contributer, Admin" },
                    { 5, true, new DateTime(2000, 12, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "martinmann@gmail.com", "Martin", true, "Ødegaard", "AQAAAAEAACcQAAAAELm4TCDPu8lY+myX02EgW9/hF1Bf63XgKr6ktCp2uNxzW2dbfBY1EAxm1VXEFhHtpA==", 0, "User, Contributer, Admin" }
                });

            migrationBuilder.InsertData(
                table: "Workout",
                columns: new[] { "Id", "Name", "Owner" },
                values: new object[] { 1, "Basic Circle Workout", "harald.rex@kongehuset.no" });

            migrationBuilder.InsertData(
                table: "ExerciseCategory",
                columns: new[] { "ExerciseId", "CategoryId" },
                values: new object[,]
                {
                    { 1, 15 },
                    { 1, 11 },
                    { 1, 8 },
                    { 1, 6 },
                    { 1, 5 },
                    { 1, 4 },
                    { 2, 1 },
                    { 3, 1 },
                    { 3, 2 }
                });

            migrationBuilder.InsertData(
                table: "GoalProgramCategory",
                columns: new[] { "GoalProgramId", "CategoryId" },
                values: new object[,]
                {
                    { 1, 15 },
                    { 1, 11 },
                    { 1, 8 },
                    { 1, 5 },
                    { 1, 4 },
                    { 1, 1 },
                    { 1, 6 }
                });

            migrationBuilder.InsertData(
                table: "GoalProgramWorkout",
                columns: new[] { "Id", "DaysFromStart", "GoalProgramId", "IsCompleted", "Owner", "WorkoutId" },
                values: new object[,]
                {
                    { 2, 2, 1, false, "harald.rex@kongehuset.no", 1 },
                    { 1, 0, 1, false, "harald.rex@kongehuset.no", 1 }
                });

            migrationBuilder.InsertData(
                table: "Profile",
                columns: new[] { "Id", "AddressId", "Disabilities", "GenderId", "Height", "MedicalConditions", "Owner", "UserId", "Weight" },
                values: new object[,]
                {
                    { 2, 2, "", 1, 198f, "", "charles.barkley@nba.com", 2, 114.5f },
                    { 6, 6, "", 2, 170f, "", "kari.nordmann@ciber.no", 6, 62f },
                    { 5, 5, "", 1, 198f, "", "martinmann@gmail.com", 5, 114.5f },
                    { 4, 4, "", 1, 187f, "", "j_johansen@hotmail.com", 4, 94.5f },
                    { 3, 3, "", 1, 193f, "", "haakon.magnus@kongehuset.no", 3, 100.5f },
                    { 1, 1, "Dysleksia, transplanted hip", 1, 187f, "Weak heart", "harald.rex@kongehuset.no", 1, 94.5f }
                });

            migrationBuilder.InsertData(
                table: "ProgramCategory",
                columns: new[] { "ProgramId", "CategoryId" },
                values: new object[,]
                {
                    { 1, 11 },
                    { 1, 8 },
                    { 1, 6 },
                    { 1, 5 },
                    { 1, 4 },
                    { 1, 1 },
                    { 1, 15 }
                });

            migrationBuilder.InsertData(
                table: "ProgramWorkout",
                columns: new[] { "Id", "DaysFromStart", "ProgramId", "WorkoutId" },
                values: new object[,]
                {
                    { 1, 0, 1, 1 },
                    { 2, 2, 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "Set",
                columns: new[] { "Id", "ExerciseId", "ExerciseRepetitions", "WorkoutId" },
                values: new object[,]
                {
                    { 2, 2, 10, 1 },
                    { 1, 1, 10, 1 },
                    { 3, 1, 10, 1 },
                    { 4, 2, 10, 1 }
                });

            migrationBuilder.InsertData(
                table: "WorkoutCategory",
                columns: new[] { "WorkoutId", "CategoryId" },
                values: new object[,]
                {
                    { 1, 8 },
                    { 1, 6 },
                    { 1, 11 },
                    { 1, 4 },
                    { 1, 1 },
                    { 1, 5 },
                    { 1, 15 }
                });

            migrationBuilder.InsertData(
                table: "Goal",
                columns: new[] { "Id", "GoalProgramId", "IsActive", "IsCompleted", "Owner", "ProfileId", "StartDate" },
                values: new object[] { 1, 1, true, false, "harald.rex@kongehuset.no", 1, new DateTime(2020, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "GoalWorkout",
                columns: new[] { "Id", "DaysFromStart", "GoalId", "IsCompleted", "Owner", "WorkoutId" },
                values: new object[] { 1, 5, 1, false, "harald.rex@kongehuset.no", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseCategory_CategoryId",
                table: "ExerciseCategory",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_GoalProgramId",
                table: "Goal",
                column: "GoalProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_ProfileId",
                table: "Goal",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalProgramCategory_CategoryId",
                table: "GoalProgramCategory",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalProgramWorkout_GoalProgramId",
                table: "GoalProgramWorkout",
                column: "GoalProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalProgramWorkout_WorkoutId",
                table: "GoalProgramWorkout",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkout_GoalId",
                table: "GoalWorkout",
                column: "GoalId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkout_WorkoutId",
                table: "GoalWorkout",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_AddressId",
                table: "Profile",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_GenderId",
                table: "Profile",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_UserId",
                table: "Profile",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramCategory_CategoryId",
                table: "ProgramCategory",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkout_ProgramId",
                table: "ProgramWorkout",
                column: "ProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkout_WorkoutId",
                table: "ProgramWorkout",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_ExerciseId",
                table: "Set",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_Set_WorkoutId",
                table: "Set",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkoutCategory_CategoryId",
                table: "WorkoutCategory",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExerciseCategory");

            migrationBuilder.DropTable(
                name: "GoalProgramCategory");

            migrationBuilder.DropTable(
                name: "GoalProgramWorkout");

            migrationBuilder.DropTable(
                name: "GoalWorkout");

            migrationBuilder.DropTable(
                name: "ProgramCategory");

            migrationBuilder.DropTable(
                name: "ProgramWorkout");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropTable(
                name: "WorkoutCategory");

            migrationBuilder.DropTable(
                name: "Goal");

            migrationBuilder.DropTable(
                name: "Program");

            migrationBuilder.DropTable(
                name: "Exercise");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Workout");

            migrationBuilder.DropTable(
                name: "GoalProgram");

            migrationBuilder.DropTable(
                name: "Profile");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "Gender");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
