﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public class WithExerciseId : Base, IWIthExerciseId
    {
        public int ExerciseId { get; set; }

        public WithExerciseId() { }
        public WithExerciseId(ISet set)
            : base(set)
        {
            ExerciseId = set.WorkoutId;
        }
    }
}
