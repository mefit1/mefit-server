﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public class Base : IBase
    {
        public ushort ExerciseRepetitions { get; set; }
        

        public Base() { }

        public Base(ISet set)
        {
            if (set == null) { throw new ArgumentNullException(); }

            ExerciseRepetitions = set.ExerciseRepetitions;
        }
    }
}
