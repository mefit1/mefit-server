﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public interface IWithFullExercise
    {
        public int Id { get; set; }
        public DTOs.Exercise.WithCategories Exercise { get; set; }
    }
}
