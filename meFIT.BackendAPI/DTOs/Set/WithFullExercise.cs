﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public class WithFullExercise : WithSetId, IWithFullExercise
    {
        public DTOs.Exercise.WithCategories Exercise { get; set; }
        public WithFullExercise(ISet set)
            :base(set)
        {
            Exercise = new DTOs.Exercise.WithCategories(set.Exercise);
        }
    }
}
