﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public interface IBase
    {
        public ushort ExerciseRepetitions { get; set; }
    }
}
