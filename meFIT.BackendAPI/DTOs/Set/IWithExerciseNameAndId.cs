﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public interface IWithExerciseNameAndId
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public string ExerciseName { get; set; }
    }
}
