﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public class WithExerciseNameAndId : WithSetId, IWithExerciseNameAndId
    {
        public int ExerciseId { get; set; }
        public string ExerciseName { get; set; }

        public WithExerciseNameAndId(ISet set)
            : base(set)
        {
            ExerciseId = set.Exercise.Id;
            ExerciseName = set.Exercise.Name;
        }
    }
}
