﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Set
{
    public class WithSetId : Base, IWithSetId
    {
        public int Id { get; set; }

        public WithSetId() { }
        public WithSetId(ISet set) 
            : base(set)
        {
            Id = set.Id;
        }
    }
}
