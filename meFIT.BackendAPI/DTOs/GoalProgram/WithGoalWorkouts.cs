﻿using meFIT.BackendAPI.DTOs.GoalProgramWorkout;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgram
{
    public class WithGoalWorkouts : IWithGoalWorkouts
    {
        public ICollection<WithWorkoutId> GoalProgramWorkouts { get; set; }

        public WithGoalWorkouts(IGoalProgram gpw)
        {
            GoalProgramWorkouts = new List<WithWorkoutId>();
            foreach(var workout in gpw.GoalProgramWorkouts)
            {
                GoalProgramWorkouts.Add(new WithWorkoutId(workout));
            }
        }
    }
}
