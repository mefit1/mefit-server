﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgram
{
    public interface IWithGoalWorkouts
    {
        public ICollection<GoalProgramWorkout.WithWorkoutId> GoalProgramWorkouts { get; set; }
    }
}
