﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgram
{
    public class WithWorkouts
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Models.Category> Categories { get; set; }
        public ICollection<DTOs.GoalProgramWorkout.WithoutGoalProgram> Workouts { get; set; }

        public WithWorkouts(IGoalProgram goalProgram)
        {
            Id = goalProgram.Id;
            Name = goalProgram.Name;
            Categories = new List<Models.Category>();

            foreach (var item in goalProgram.GoalProgramCategories)
            {
                Categories.Add(item.Category);
            }

            Workouts = new List<DTOs.GoalProgramWorkout.WithoutGoalProgram>();

            foreach (var item in goalProgram.GoalProgramWorkouts)
            {
                Workouts.Add(new DTOs.GoalProgramWorkout.WithoutGoalProgram(item));
            }
        }
    }
}
