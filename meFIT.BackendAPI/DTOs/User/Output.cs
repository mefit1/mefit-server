﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class Output
    {
        public string Token { get; set; }
        public string Role { get; set; }
    }
}
