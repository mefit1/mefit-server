﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IBase
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime DOB { get; set; }
    }
}
