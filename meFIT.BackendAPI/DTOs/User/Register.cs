﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class Register : WithEmail, IRegister
    {
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public Profile.Post Profile { get; set; }
    }
}
