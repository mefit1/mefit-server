﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class Base : IBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }

        public Base() { }

        public Base(IUser user)
        {
            if (user == null) throw new ArgumentNullException();

            FirstName = user.FirstName;
            LastName = user.LastName;
            DOB = user.DOB;
        }
    }
}
