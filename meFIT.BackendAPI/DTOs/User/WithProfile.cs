﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class WithProfile : WithRights, IWithProfile
    {
        public Profile.WithAddress Profile { get; set; }
        public Gender.Base Gender { get; set; }

        public WithProfile() { }
        public WithProfile(IUser user)
            : base(user)
        {
            Gender = new DTOs.Gender.Base(user.Profile.Gender);
            Profile = new DTOs.Profile.WithAddress(user.Profile);
        }
    }
}
