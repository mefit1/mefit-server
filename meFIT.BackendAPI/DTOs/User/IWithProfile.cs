﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IWithProfile
    {
        public DTOs.Gender.Base Gender { get; set; }
        public DTOs.Profile.WithAddress Profile { get; set; }
    }
}
