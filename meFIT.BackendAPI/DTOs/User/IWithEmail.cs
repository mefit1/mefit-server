﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IWithEmail
    {
        public string Email { get; set; }
    }
}
