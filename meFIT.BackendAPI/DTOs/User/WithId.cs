﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class WithId : WithEmail, IWithId
    {
        public int Id { get; set; }

        public WithId() { }
        public WithId(IUser user)
            : base(user)
        {
            Id = user.Id;
        }
    }
}
