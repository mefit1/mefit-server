﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class UpdateUser : Base, IUpdateUser
    {
        public Profile.Post Profile { get; set; }
        public UpdateUser() { }

        //Update user with values from dto
        public Models.IUser Update(IUser user)
        {
            user.Profile.Address.AddressLine1 = Profile.Address.AddressLine1;
            user.Profile.Address.AddressLine2 = Profile.Address.AddressLine2;
            user.Profile.Address.AddressLine3 = Profile.Address.AddressLine3;
            user.Profile.Address.City = Profile.Address.City;
            user.Profile.Address.Country = Profile.Address.Country;
            user.Profile.Address.PostalCode= Profile.Address.PostalCode;

            user.Profile.Weight = Profile.Weight;
            user.Profile.Height = Profile.Height;
            user.Profile.MedicalConditions = Profile.MedicalConditions;
            user.Profile.GenderId = Profile.GenderId;
            user.Profile.Disabilities = Profile.Disabilities;

            user.FirstName = FirstName;
            user.LastName = LastName;
            user.DOB = DOB;
            
            return user;
        }
    }
}
