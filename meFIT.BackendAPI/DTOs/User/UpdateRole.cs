﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class UpdateRole : IUpdateRole
    {
        public string Role { get; set; }

        public UpdateRole() { }
    }
}
