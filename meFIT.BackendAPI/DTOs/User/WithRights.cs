﻿using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class WithRights : WithId, IWithRights
    {
        public string Role { get; set; }
        public bool ApplyForContributor { get; set; }

        public WithRights() { }
        public WithRights(IUser user)
            :base(user)
        {
            Role = user.Role;
            ApplyForContributor = user.ApplyForContributor;
        }
    }
}
