﻿using meFIT.BackendAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IWithRights
    {
        public string Role { get; set; }
        public bool ApplyForContributor { get; set; }
    }
}
