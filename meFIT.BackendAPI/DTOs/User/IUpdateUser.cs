﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IUpdateUser
    {
        public Profile.Post Profile { get; set; }
    }
}
