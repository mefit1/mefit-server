﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public interface IRegister : IBase
    {
        string Password { get; set; }
        string PasswordConfirmation { get; set; }
        DTOs.Profile.Post Profile { get; set; }
    }
}
