﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class UpdatePassword : IUpdatePassword
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public UpdatePassword() { }
    }
}
