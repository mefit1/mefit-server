﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.User
{
    public class WithEmail : Base, IWithEmail
    {
        public string Email { get; set; }
        public WithEmail() { }
        public WithEmail(IUser user)
            : base(user)
        {
            Email = user.Email;
        }
    }
}
