﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramCategory
{
    public interface IBase
    {
        public int ProgramId { get; set; }
        public int CategoryId { get; set; }
    }
}
