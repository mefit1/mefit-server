﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramCategory
{
    public class Base : IBase
    {
        public int ProgramId { get; set; }
        public int CategoryId { get; set; }

        public Base() { }
        public Base(int programId, int categoryId)
        {
            ProgramId = programId;
            CategoryId = categoryId;
        }
    }
}
