﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramWorkout
{
    public class Base : IBase
    {
        public ushort DaysFromStart { get; set; }
        public int WorkoutId { get; set; }

        public Base()
        {

        }

        public Base(IProgramWorkout programWorkout)
        {
            if(programWorkout == null) { throw new ArgumentNullException(); } 

            DaysFromStart = programWorkout.DaysFromStart;
            WorkoutId = programWorkout.WorkoutId;
        } 
    }
}
