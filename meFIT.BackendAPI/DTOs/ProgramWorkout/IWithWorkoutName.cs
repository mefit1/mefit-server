﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramWorkout
{
    public interface IWithWorkoutName 
    {
        public int Id { get; set; }
        public string WorkoutName { get; set; }
    }
}
