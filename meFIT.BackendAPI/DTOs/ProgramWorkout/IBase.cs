﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramWorkout
{
    public interface IBase
    {
        public ushort DaysFromStart { get; set; }
        public int WorkoutId { get; set; }
    }
}
