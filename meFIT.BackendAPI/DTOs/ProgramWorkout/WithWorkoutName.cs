﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.ProgramWorkout
{
    public class WithWorkoutName : Base, IWithWorkoutName
    {
        public string WorkoutName { get; set; }
        public int Id { get; set; }

        public WithWorkoutName(IProgramWorkout programWorkout)
            :base(programWorkout)
        {
            Id = programWorkout.Id;
            WorkoutName = programWorkout.Workout.Name;
        }
    }
}
