﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Gender
{
    public class Base : IBase
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Base() { }
        public Base(IGender gender)
        {
            Id = gender.Id;
            Name = gender.Name;
        }
    }
}
