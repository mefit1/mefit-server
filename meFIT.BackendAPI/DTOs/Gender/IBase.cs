﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Gender
{
    public interface IBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
