﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public interface IPost
    {
        public int GoalId { get; set; }
    }
}
