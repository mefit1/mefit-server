﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public class Base : IBase
    {
        public int WorkoutId { get; set; }
        public ushort DaysFromStart { get; set; }

        public Base() { }
        public Base(IGoalWorkoutCommon goalWorkout)
        {
            if (goalWorkout == null) throw new ArgumentNullException();

            WorkoutId = goalWorkout.WorkoutId;
            DaysFromStart = goalWorkout.DaysFromStart;
        }
    }
}
