﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public class WithCompleted : Base, IWithCompleted
    {
        public bool IsCompleted { get; set; }

        public WithCompleted() { }
        public WithCompleted(IGoalWorkoutCommon workout)
            : base(workout)
        {
            IsCompleted = workout.IsCompleted;
        }
    }
}
