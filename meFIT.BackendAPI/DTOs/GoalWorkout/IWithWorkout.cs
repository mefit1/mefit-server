﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public interface IWithWorkout
    {
        DTOs.Workout.WithCategory Workout { get; }
        int Id { get; }
    }
}
