﻿namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public interface IWithCompleted : IBase
    {
        bool IsCompleted { get; set; }
    }
}