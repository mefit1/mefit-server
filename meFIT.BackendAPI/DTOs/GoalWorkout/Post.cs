﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public class Post : Base, IPost
    {
        public int GoalId { get; set; }
        public Post() { }

    }
}
