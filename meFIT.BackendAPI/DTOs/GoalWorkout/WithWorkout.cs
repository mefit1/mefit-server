﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalWorkout
{
    public class WithWorkout : WithCompleted, IWithWorkout
    {
        public Workout.WithCategory Workout { get; set; }

        public int Id { get; set; }

        public WithWorkout(IGoalWorkoutCommon goalWorkout)
            : base(goalWorkout)
        {
            Workout = new Workout.WithCategory(goalWorkout.Workout);
            Id = goalWorkout.Id;
        }
    }
}
