﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgramWorkout
{
    public interface IWithWorkoutId
    {
        int Id { get; set; }
        int WorkoutId { get; set; }
        int DaysFromStart { get; set; }
    }
}
