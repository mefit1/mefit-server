﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgramWorkout
{
    public class WithoutGoalProgram
    {
        public int Id { get; set; }
        public bool IsCompleted { get; set; }
        public ushort DaysFromStart { get; set; }
        public DTOs.Workout.WithoutSets Workout { get; set; }

        public WithoutGoalProgram() { }

        public WithoutGoalProgram(IGoalProgramWorkout gpw)
        {
            Id = gpw.Id;
            IsCompleted = gpw.IsCompleted;
            DaysFromStart = gpw.DaysFromStart;
            Workout = new DTOs.Workout.WithoutSets(gpw.Workout);
        }
    }
}
