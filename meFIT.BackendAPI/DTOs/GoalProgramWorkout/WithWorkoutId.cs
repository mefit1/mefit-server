﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.GoalProgramWorkout
{
    public class WithWorkoutId : IWithWorkoutId
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int DaysFromStart { get; set; }

        public WithWorkoutId(IGoalProgramWorkout gpw)
        {
            Id = gpw.Id;
            WorkoutId = gpw.WorkoutId;
            DaysFromStart = gpw.DaysFromStart;
        }
    }
}
