﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class WithId : Base, IWithId
    {
        public int Id { get; set; }

        public WithId() { }
        public WithId(IProfile profile)
            : base(profile)
        {
            Id = profile.Id;
        }
    }
}
