﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class Base : IBase
    {
        public float Weight { get; set; }
        public float Height { get; set; }
        public string MedicalConditions { get; set; }
        public string Disabilities { get; set; }

        public Base() { }

        public Base(IProfile profile)
        {
            Weight = profile.Weight;
            Height = profile.Height;
            MedicalConditions = profile.MedicalConditions;
            Disabilities = profile.Disabilities;
        }
    }
}
