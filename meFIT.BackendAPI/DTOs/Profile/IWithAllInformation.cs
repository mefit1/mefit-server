﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public interface IWithAllInformation
    {
        public ICollection<DTOs.Goal.Base> Goals { get; set; }
        public DTOs.User.WithEmail User { get; set; }
        public DTOs.Address.Base Address { get; set; }
        public DTOs.Gender.Base Gender { get; set; }
        public int Id { get; set; }
    }
}
