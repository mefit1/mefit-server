﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public interface IPost
    {
        public int GenderId { get; set; }
        public DTOs.Address.Base Address { get; set; }
    }
}
