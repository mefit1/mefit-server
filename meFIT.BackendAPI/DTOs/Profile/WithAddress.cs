﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class WithAddress : WithId, IWithAddress
    {
        public Address.Base Address { get; set; }

        public WithAddress() { }
        public WithAddress(IProfile profile)
            : base(profile)
        {
            Address = new Address.Base(profile.Address);
        }
    }
}
