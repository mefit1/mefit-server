﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    interface IBase
    {
        float Weight { get; set; }
        float Height { get; set; }
        string MedicalConditions { get; set; }
        string Disabilities { get; set; }
    }
}
