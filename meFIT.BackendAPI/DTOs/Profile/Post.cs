﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class Post : Base, IPost
    {
        public int GenderId { get; set; }
        public DTOs.Address.Base Address { get; set; }

        public Post() { }
        public Post(IProfile profile) 
            : base(profile)
        {
            GenderId = profile.GenderId;
            Address = new DTOs.Address.Base(profile.Address);
        }
    }
}
