﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Profile
{
    public class WithAllInformation : Base, IWithAllInformation
    {
        public ICollection<Goal.Base> Goals { get; set; }
        public User.WithEmail User { get; set; }
        public Address.Base Address { get; set; }
        public Gender.Base Gender { get; set; }
        public int Id { get; set; }

        public WithAllInformation(IProfile profile)
            : base(profile)
        {
            Id = profile.Id;
            Goals = new List<Goal.Base>();
            foreach(var goal in profile.Goals)
            {
                Goals.Add(new DTOs.Goal.Base(goal));
            }

            User = new User.WithEmail(profile.User);
            Gender = new Gender.Base(profile.Gender);

            if (profile.Address != null) 
            {
                Address = new Address.Base(profile.Address);
            }
        }
    }
}
