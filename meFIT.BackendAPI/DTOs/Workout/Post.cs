﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class Post : Base, IPost
    {
        public ICollection<int> CategoryIds { get; set; }
        public ICollection<Set.WithExerciseId> Sets { get; set; }

        public Post() { }
    }
}
