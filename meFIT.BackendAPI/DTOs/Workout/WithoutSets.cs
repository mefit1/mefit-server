﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WithoutSets : Base
    {
        public int Id { get; set; }
        public List<Models.Category> WorkoutCategories { get; set; }

        public WithoutSets() { }

        public WithoutSets(IWorkout workout)
            :base(workout)
        {
            Id = workout.Id;
            WorkoutCategories = new List<Models.Category>();

            foreach (var workcat in workout.WorkoutCategories)
            {
                WorkoutCategories.Add(workcat.Category);
            }
        }
    }
}
