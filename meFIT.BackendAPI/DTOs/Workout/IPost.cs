﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public interface IPost
    {
        public ICollection<int> CategoryIds { get; set; }
        public ICollection<DTOs.Set.WithExerciseId> Sets { get; set; }
    }
}
