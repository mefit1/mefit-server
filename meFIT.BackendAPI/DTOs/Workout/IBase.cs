﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public interface IBase
    {
        public string Name { get; set; }
    }
}
