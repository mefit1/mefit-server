﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public interface IWithSets
    {
        public int Id { get; set; }
        public ICollection<DTOs.Set.IWithExerciseNameAndId> Sets { get; set; }
    }
}
