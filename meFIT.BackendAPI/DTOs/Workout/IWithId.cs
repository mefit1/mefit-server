﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public interface IWithId
    {
        public int Id { get; set; }
    }
}
