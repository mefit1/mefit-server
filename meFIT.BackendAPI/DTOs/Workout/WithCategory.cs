﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WithCategory : WithId, IWithCategories
    {
        public ICollection<Category.Base> Category { get; set; }

        public WithCategory() { }
        public WithCategory(IWorkout workout)
            : base(workout)
        {
            Category = new List<Category.Base>();
            foreach (var category in workout.WorkoutCategories)
            {
                Category.Add(new DTOs.Category.Base(category.Category));
            }
        }
    }
}
