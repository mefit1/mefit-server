﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class Base : IBase
    {
        public string Name { get; set; }

        public Base() { }

        public Base(IWorkout workout)
        {
            if(workout == null) { throw new ArgumentNullException(); }
            Name = workout.Name;
        }
    }
}
