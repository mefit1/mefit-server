﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WithFullExercise : WithCategory, IWithFullExercise
    {
        public int Id { get; set; }
        public ICollection<DTOs.Set.IWithFullExercise> Sets { get; set; }

        public WithFullExercise(IWorkout workout)
            :base(workout)
        {
            Sets = new List<DTOs.Set.IWithFullExercise>();
            foreach (var set in workout.Sets)
            {
                Sets.Add(new DTOs.Set.WithFullExercise(set));
            }
        }

    }
}
