﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WithSets : WithCategory, IWithSets
    {
        public int Id { get; set; }
        public ICollection<DTOs.Set.IWithExerciseNameAndId> Sets { get; set; }

        public WithSets(IWorkout workout)
            :base(workout)
        {
            Id = workout.Id;
            Sets = new List<DTOs.Set.IWithExerciseNameAndId>();
            foreach (var set in workout.Sets)
            {
                Sets.Add(new DTOs.Set.WithExerciseNameAndId(set));
            }
        }
    }
}
