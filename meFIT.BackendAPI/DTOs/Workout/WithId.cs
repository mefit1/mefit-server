﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Workout
{
    public class WithId : Base, IWithId
    {
        public int Id { get ; set ; }
        public WithId() { }
        public WithId(IWorkout workout)
            : base(workout)
        {
            Id = workout.Id;
        }
    }
}
