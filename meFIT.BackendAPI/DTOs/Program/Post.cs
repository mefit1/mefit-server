﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public class Post : Base, IPost
    {
        public ICollection<int> CategoryIds { get; set; }
        public ICollection<ProgramWorkout.Base> ProgramWorkouts { get; set; }

        public Post() { }
    }
}
