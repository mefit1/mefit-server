﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public interface IWithWorkout : IWithCategory
    {
        public ICollection<DTOs.ProgramWorkout.WithWorkoutName> Workouts { get; set; }
    }
}
