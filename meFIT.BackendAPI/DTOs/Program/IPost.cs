﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public interface IPost
    {
        public ICollection<int> CategoryIds { get; set; }
        public ICollection<DTOs.ProgramWorkout.Base> ProgramWorkouts { get; set; }
    }
}
