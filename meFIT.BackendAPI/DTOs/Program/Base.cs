﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public class Base : IBase
    {
        public string Name { get; set; }

        public Base() { }

        public Base(IProgram program)
        {
            if(program == null) { throw new ArgumentNullException(); }

            Name = program.Name;
        }
    }
}
