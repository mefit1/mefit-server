﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public class WithCategory : Base, IWithCategory
    {
        public int Id { get; set; }
        public ICollection<Category.Base> Category { get; set; }
        public WithCategory(IProgram program)
            :base(program)
        {
            Id = program.Id;
            Category = new List<Category.Base>();
            foreach (var category in program.ProgramCategories)
            {
                Category.Add(new DTOs.Category.Base(category.Category));
            }
        }
    }
}
