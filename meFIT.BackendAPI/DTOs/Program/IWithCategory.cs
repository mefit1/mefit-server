﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public interface IWithCategory
    {
        public int Id { get; set; }
        public ICollection<DTOs.Category.Base> Category { get; set; }
    }
}
