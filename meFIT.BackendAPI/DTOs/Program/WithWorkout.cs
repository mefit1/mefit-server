﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Program
{
    public class WithWorkout : WithCategory, IWithWorkout
    {
        public ICollection<ProgramWorkout.WithWorkoutName> Workouts { get; set; }

        public WithWorkout (IProgram program)
            :base(program)
        {
            Workouts = new List<ProgramWorkout.WithWorkoutName>();
            foreach(var workout in program.ProgramWorkouts)
            {
                Workouts.Add(new DTOs.ProgramWorkout.WithWorkoutName(workout));
            }
            
        }
    }
}
