﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Exercise
{
    public class Base : IBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }

        public Base() { }

        public Base(IExercise exercise)
        {
            if(exercise == null) { throw new ArgumentNullException(); }

            Name = exercise.Name;
            Description = exercise.Description;
            Image = exercise.Image;
            Video = exercise.Video;
        }
    }
}
