﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Exercise
{
    public interface IBase
    {
        string Name { get; }
        string Description { get; }
        string Image { get; }
        string Video { get; }
    }
}
