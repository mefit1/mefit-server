﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Exercise
{
    public class WithCategories : Base
    {
        public int Id { get; set; }
        public ICollection<Models.Category> Categories { get; set; }

        public WithCategories(IExercise exercise)
            : base(exercise)
        {
            Id = exercise.Id;
            Categories = exercise.TargetMuscleGroups.Select(excat => excat.Category).ToList();
        }
    }
}
