﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Exercise
{
    public class WithCategoriesPost : Base
    {
        public ICollection<int> TargetMuscleGroups { get; set; }

        public WithCategoriesPost()
        {

        }

        public WithCategoriesPost(IExercise exercise)
            :base(exercise)
        {
            //Converting ICollection<ExerciseCategory> to List<Category>
            TargetMuscleGroups = new List<int>();
            foreach (var muscleGroup in exercise.TargetMuscleGroups)
            {
                TargetMuscleGroups.Add(muscleGroup.Category.Id);
            }
        }
    }
}
