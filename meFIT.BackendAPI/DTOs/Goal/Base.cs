﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public class Base : IBase
    {
        public DateTime StartDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsCompleted { get; set; }

        public Base() { }

        public Base(IGoal goal)
        {
            if(goal == null) { throw new ArgumentNullException(); }
            StartDate = goal.StartDate;
            IsActive = goal.IsActive;
            IsCompleted = goal.IsCompleted;
        }
    }
}
