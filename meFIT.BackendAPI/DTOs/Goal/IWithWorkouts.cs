﻿using System.Collections.Generic;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public interface IWithWorkouts : IBase
    {
        int ProfileId { get; set; }
        DTOs.GoalProgram.WithWorkouts GoalProgram { get; set; }
        ICollection<DTOs.GoalWorkout.WithWorkout> GoalWorkouts { get; set; }
    }
}