﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public class BaseWithId : Base
    {
        public int Id { get; set; }

        public BaseWithId() { }

        public BaseWithId(IGoal goal)
            :base(goal)
        {
            Id = goal.Id;
        }
    }
}
