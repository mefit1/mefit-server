﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public class WithGoalWorkoutsPost : IWithGoalWorkoutsPost
    {
        public int ProfileId { get; set; }
        public int? GoalProgramId { get; set; }
        public ICollection<DTOs.GoalWorkout.Base> GoalWorkouts { get; set; }
        public DateTime StartDate { get; set; }

        public WithGoalWorkoutsPost() { }
    }
}
