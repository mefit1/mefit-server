﻿using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.GoalProgram;
using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public class WithWorkouts : Base, IWithWorkouts
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public DTOs.GoalProgram.WithWorkouts GoalProgram { get; set; }
        public ICollection<DTOs.GoalWorkout.WithWorkout> GoalWorkouts { get; set; }

        public WithWorkouts() { }

        public WithWorkouts(IGoal goal)
            :base(goal)
        {
            Id = goal.Id;
            ProfileId = goal.ProfileId;
            GoalWorkouts = new List<DTOs.GoalWorkout.WithWorkout>();
            GoalProgram = goal.GoalProgram == null ? null : new DTOs.GoalProgram.WithWorkouts(goal.GoalProgram);
            foreach (var goalWorkout in goal.GoalWorkouts)
            {
                GoalWorkouts.Add(new DTOs.GoalWorkout.WithWorkout(goalWorkout));
            }
        }
    }
}
