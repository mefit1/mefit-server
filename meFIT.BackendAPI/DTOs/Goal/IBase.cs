﻿using System;

namespace meFIT.BackendAPI.DTOs.Goal
{
    public interface IBase
    {
        DateTime StartDate { get; set; }
        bool IsActive { get; set; }
        bool IsCompleted { get; set; }
    }
}