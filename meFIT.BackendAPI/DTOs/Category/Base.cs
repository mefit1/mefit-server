﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Category
{
    public class Base : IBase
    {
        public int Id { get; set; }
        public string ConcreteName { get; set; }
        public string GeneralName { get; set; }

        public Base(ICategory category)
        {
            Id = category.Id;
            ConcreteName = category.ConcreteName;
            GeneralName = category.GeneralName;
        }
    }
}
