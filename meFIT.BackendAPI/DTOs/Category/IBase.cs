﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DTOs.Category
{
    public interface IBase
    {
        public int Id { get; set; }
        public string ConcreteName { get; set; }
        public string GeneralName { get; set; }
    }
}
