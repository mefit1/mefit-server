﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IProfile : IHasOwner
    {
        int Id { get; set; }
        int UserId { get; set; }
        User User { get; set; }
        int? AddressId { get; set; }
        Address Address { get; set; }
        float Weight { get; set; }
        float Height { get; set; }
        string MedicalConditions { get; set; }
        string Disabilities { get; set; }
        ICollection<Goal> Goals { get; set; }
        int GenderId { get; set; }
        Gender Gender { get; set; }
    }
}
