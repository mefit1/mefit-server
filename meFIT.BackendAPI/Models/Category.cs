﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Category</c> models a category which is used in program, workout, and exercise.
    /// </summary>
    public class Category : ICategory
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string ConcreteName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string GeneralName { get; set; }
    }
}
