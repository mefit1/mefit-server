﻿using System.ComponentModel.DataAnnotations;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>GoalProgramWorkout</c> is a joining table between <c>GoalProgram</c> and <c>Workout</c>.
    /// </summary>
    public class GoalProgramWorkout : IGoalProgramWorkout
    {
        [Key]
        public int Id { get; set; }
        public string Owner { get; set; }
        public bool IsCompleted { get; set; }
        public ushort DaysFromStart { get; set; }

        public int GoalProgramId { get; set; }
        public int WorkoutId { get; set; }

        public GoalProgram GoalProgram { get; set; }
        public Workout Workout { get; set; }
    }
}