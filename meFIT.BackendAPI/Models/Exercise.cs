﻿using meFIT.BackendAPI.DTOs.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Exercise</c> models an exercise.
    /// </summary>
    public class Exercise : IExercise
    {
        [Key]
        public int Id { get; set; }

        public string Owner { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(255)]
        public string Image { get; set; }

        [StringLength(255)]
        public string Video { get; set; }

        public ICollection<ExerciseCategory> TargetMuscleGroups { get; set; }

        public Exercise() { }

        public Exercise(IBase exercise)
        {
            if(exercise == null) { throw new ArgumentNullException(); }

            Name = exercise.Name;
            Description = exercise.Description;
            Image = exercise.Image;
            Video = exercise.Video;
        }
    }
}
