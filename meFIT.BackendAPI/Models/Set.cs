﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Set</c> models repetitions of an <c>Exercise</c> and functions 
    /// as a joining table between <c>Exercise</c> and <c>Workout</c>.
    /// </summary>
    public class Set : ISet
    {
        [Key]
        public int Id { get; set; }
        public ushort ExerciseRepetitions { get; set; }

        public int ExerciseId { get; set; }
        public int WorkoutId { get; set; }

        public Exercise Exercise { get; set; }
        public Workout Workout { get; set; }

        public Set() { }
        public Set(DTOs.Set.WithExerciseId set)
        {
            ExerciseRepetitions = set.ExerciseRepetitions;
            ExerciseId = set.ExerciseId;
        }
    }
}
