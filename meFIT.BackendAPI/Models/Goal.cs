﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Goal</c> models a set of workouts. 
    /// </summary>
    public class Goal : IGoal
    {
        [Key]
        public int Id { get; set; }

        public string Owner { get; set; }

        public int? GoalProgramId { get; set; }
        public GoalProgram GoalProgram { get; set; }
        public ICollection<GoalWorkout> GoalWorkouts { get; set; }

        public int ProfileId { get; set; }
        public Profile Profile { get; set; }    
        public DateTime StartDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsCompleted { get; set; }

        public Goal() { }

        public Goal(DTOs.Goal.IWithGoalWorkoutsPost goal)
        {
            if (goal == null) { throw new ArgumentNullException(); }

            GoalProgramId = goal.GoalProgramId;
            ProfileId = goal.ProfileId;
            StartDate = goal.StartDate;
            IsActive = true;
            IsCompleted = false;
            GoalWorkouts = new List<GoalWorkout>();

            foreach (var gw in goal.GoalWorkouts)
            {
                GoalWorkouts.Add(new GoalWorkout(gw));
            }
        }
    }
}
