﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IProgram : IHasOwner
    {
        int Id { get; set; }
        string Name { get; set; }
        ICollection<ProgramWorkout> ProgramWorkouts { get; set; }
        ICollection<ProgramCategory> ProgramCategories { get; set; }
    }
}
