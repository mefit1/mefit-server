﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>GoalProgram</c> models a program used in a <c>Goal</c>.
    /// </summary>
    public class GoalProgram : IGoalProgram
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        public ICollection<GoalProgramWorkout> GoalProgramWorkouts { get; set; }
        public ICollection<GoalProgramCategory> GoalProgramCategories { get; set; }
    }
}
