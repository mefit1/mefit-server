﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>ProgramWorkout</c> models a joining table between <c>Program</c> and <c>Workout</c>.
    /// </summary>
    public class ProgramWorkout : IProgramWorkout
    {
        [Key]
        public int Id { get; set; }
        public ushort DaysFromStart { get; set; }
        public int ProgramId { get; set; }
        public int WorkoutId { get; set; }

        public Program Program { get; set; }
        public Workout Workout { get; set; }

        public ProgramWorkout() { }
        public ProgramWorkout(DTOs.ProgramWorkout.IBase programWorkout)
        {
            DaysFromStart = programWorkout.DaysFromStart;
            WorkoutId = programWorkout.WorkoutId;
        }
    }
}
