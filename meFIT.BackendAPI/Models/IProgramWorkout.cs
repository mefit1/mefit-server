﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IProgramWorkout
    {
        public int Id { get; set; }
        public ushort DaysFromStart { get; set; }
        public int ProgramId { get; set; }
        public int WorkoutId { get; set; }

        public Program Program { get; set; }
        public Workout Workout { get; set; }
    }
}
