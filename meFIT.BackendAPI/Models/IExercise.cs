﻿using meFIT.BackendAPI.DTOs.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IExercise : IBase, IHasOwner
    {
        int Id { get; set; }
        ICollection<ExerciseCategory> TargetMuscleGroups { get; set; }
    }
}
