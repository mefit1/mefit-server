﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IUser
    {
        int Id { get; set; }
        string Password { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime DOB { get; set; }
        string Email { get; set; }
        string Role { get; set; }
        bool ApplyForContributor { get; set; }
        int ProfileId { get; set; }
        Profile Profile { get; set; }
        bool IsConfirmed { get; set; }
    }
}
