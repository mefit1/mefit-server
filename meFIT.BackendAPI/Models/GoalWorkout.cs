﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>GoalWorkout</c> joins class <c>Goal</c> and class <c>Workout</c>.
    /// </summary>
    public class GoalWorkout : IGoalWorkout
    {
        [Key]
        public int Id { get; set; }
        public string Owner { get; set; }
        public bool IsCompleted { get; set; }
        public ushort DaysFromStart { get; set; }

        public int GoalId { get; set; }
        public int WorkoutId { get; set; }

        public Goal Goal { get; set; }
        public Workout Workout { get; set; }

        public GoalWorkout() { }

        public GoalWorkout(DTOs.GoalWorkout.IBase gw)
        {
            WorkoutId = gw.WorkoutId;
            DaysFromStart = gw.DaysFromStart;
            IsCompleted = false;
        }

        public GoalWorkout(DTOs.GoalWorkout.Post gw)
        {
            WorkoutId = gw.WorkoutId;
            DaysFromStart = gw.DaysFromStart;
            IsCompleted = false;
            GoalId = gw.GoalId;
        }
    }
}
