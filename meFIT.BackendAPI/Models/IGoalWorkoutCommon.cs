﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IGoalWorkoutCommon : IHasOwner
    {
        int Id { get; set; }
        bool IsCompleted { get; set; }
        ushort DaysFromStart { get; set; }
        int WorkoutId { get; set; }
        Workout Workout { get; set; }
    }
}
