﻿using meFIT.BackendAPI.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    //TODOS: Add email-address?

    /// <summary>
    /// Class <c>User</c> models the identity of a user and its roles.
    /// </summary>
    public class User : IUser
    {
        [Key]
        public int Id { get; set; }
        public string Password { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [Range(typeof(DateTime), "01/01/1900", "01/01/2050")]
        public DateTime DOB { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public bool ApplyForContributor { get; set; } = false;

        public string Role { get; set; } = "User";

        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public bool IsConfirmed { get; set; } = false;

        public User() { }
        public User(DTOs.User.Register user)
        {
            if (user == null) { throw new ArgumentNullException(); }

            Password = user.Password;
            FirstName = user.FirstName;
            LastName = user.LastName;
            DOB = user.DOB;
            Email = user.Email;
        }
    }
}
