﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface ISet
    {
        public int Id { get; set; }
        public ushort ExerciseRepetitions { get; set; }

        public int ExerciseId { get; set; }
        public int WorkoutId { get; set; }

        public Exercise Exercise { get; set; }
        public Workout Workout { get; set; }
    }
}
