﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public class ProgramCategory
    {
        public int CategoryId { get; set; }
        public int ProgramId { get; set; }

        public Category Category { get; set; }
        public Program Program { get; set; }
        public ProgramCategory() { }
        public ProgramCategory(int categoryId)
        {
            CategoryId = categoryId;
        }
    }
}
