﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public class WorkoutCategory
    {
        public int CategoryId { get; set; }
        public int WorkoutId { get; set; }

        public Category Category { get; set; }
        public Workout Workout { get; set; }

        public WorkoutCategory() { }
        public WorkoutCategory(int categoryId)
        {
            CategoryId = categoryId;
        }
    }
}
