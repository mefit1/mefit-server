﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IGoalWorkout : IGoalWorkoutCommon
    {
        int GoalId { get; set; }
        Goal Goal { get; set; }
    }
}
