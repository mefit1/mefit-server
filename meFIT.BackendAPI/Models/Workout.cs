﻿using meFIT.BackendAPI.DTOs.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Workout</c> models a workout.
    /// </summary>
    public class Workout : IWorkout
    {
        [Key]
        public int Id { get; set; }
        public string Owner { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        public ICollection<Set> Sets { get; set; }
        public ICollection<WorkoutCategory> WorkoutCategories { get; set; }

        public Workout() { }
        public Workout(DTOs.Workout.Post workout)
        {
            Name = workout.Name;
            WorkoutCategories = new List<WorkoutCategory>();
            Sets = new List<Set>();

            foreach(var category in workout.CategoryIds)
            {
                WorkoutCategories.Add(new WorkoutCategory(category));
            }

            foreach(var set in workout.Sets)
            {
                Sets.Add(new Set(set));
            }
        }
    }
}
