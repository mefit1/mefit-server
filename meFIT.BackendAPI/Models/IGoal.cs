﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IGoal : IHasOwner
    {
        int Id { get; set; }
        int? GoalProgramId { get; set; }
        GoalProgram GoalProgram { get; set; }
        ICollection<GoalWorkout> GoalWorkouts { get; set; }
        int ProfileId { get; set; }
        Profile Profile { get; set; }
        DateTime StartDate { get; set; }
        bool IsActive { get; set; }
        bool IsCompleted { get; set; }
    }
}
