﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public class GoalProgramCategory : IGoalProgramCategory
    {
        public int GoalProgramId { get; set; }
        public int CategoryId { get; set; }

        public GoalProgram GoalProgram { get; set; }
        public Category Category { get; set; }
    }
}
