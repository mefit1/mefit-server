﻿using meFIT.BackendAPI.DTOs.Program;
using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    /// <summary>
    /// Class <c>Program</c> models a workout program.
    /// </summary>
    public class Program : IProgram
    {
        [Key]
        public int Id { get; set; }
        public string Owner { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }
        public ICollection<ProgramWorkout> ProgramWorkouts { get; set; }
        public ICollection<ProgramCategory> ProgramCategories { get; set; }

        public Program() { }
        public Program(IBase program)
        {
            if (program == null) { throw new ArgumentNullException(); }

            Name = program.Name;
        }

        public Program(Post program)
        {
            Name = program.Name;

            //Adds all the categories to the program
            ProgramCategories = new List<ProgramCategory>();
            foreach (var category in program.CategoryIds)
            {
                ProgramCategories.Add(new ProgramCategory(category));
            }

            ProgramWorkouts = new List<ProgramWorkout>();
            foreach (var workout in program.ProgramWorkouts)
            {
                ProgramWorkouts.Add(new ProgramWorkout(workout));
            }
        }
    }
}
