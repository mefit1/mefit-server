﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface ICategory
    {
        public int Id { get; set; }
        public string ConcreteName { get; set; }
        public string GeneralName { get; set; }
    }
}
