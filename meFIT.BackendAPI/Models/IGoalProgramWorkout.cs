﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IGoalProgramWorkout : IGoalWorkoutCommon
    {
        int GoalProgramId { get; set; }
        GoalProgram GoalProgram { get; set; }
    }
}
