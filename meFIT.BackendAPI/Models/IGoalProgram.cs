﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IGoalProgram
    {
        int Id { get; set; }
        public string Name { get; set; }
        public ICollection<GoalProgramWorkout> GoalProgramWorkouts { get; set; }
        public ICollection<GoalProgramCategory> GoalProgramCategories { get; set; }
    }
}
