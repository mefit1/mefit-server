﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Models
{
    public interface IWorkout : IHasOwner
    {
        int Id { get; set; }
        string Name { get; set; }
        ICollection<Set> Sets { get; set; }
        ICollection<WorkoutCategory> WorkoutCategories { get; set; }
    }
}
