﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Handlers
{
    public class UserAuthorizationHandler : AuthorizationHandler<AuthorizationRequirement, User>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, 
                                                       AuthorizationRequirement requirement, 
                                                       User resource)
        {
            if (context.User.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value == resource.Email)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
