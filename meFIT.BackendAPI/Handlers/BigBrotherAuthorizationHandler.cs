﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Handlers
{
    public class BigBrotherAuthorizationHandler : AuthorizationHandler<BigBrotherAuthorizationRequirement, User>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, 
                                                       BigBrotherAuthorizationRequirement requirement, 
                                                       User resource)
        {
            if (context.User.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value == resource.Email ||
                context.User.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Role).Value == Helpers.Role.Admin)
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }

    public class BigBrotherAuthorizationRequirement : IAuthorizationRequirement { }
}
