﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Handlers
{
    public class OwnerAuthorizationHandler<T> 
        : AuthorizationHandler<AuthorizationRequirement, T>
        where T : IHasOwner
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, 
                                                       AuthorizationRequirement requirement,
                                                       T resource)
        {
            if (context.User.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value == resource.Owner)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }

    public class AuthorizationRequirement : IAuthorizationRequirement { }
}
