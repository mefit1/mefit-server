﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.DTOs.Program;
using meFIT.BackendAPI.DTOs.User;
using meFIT.BackendAPI.DTOs.Workout;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/workouts")]
    [ApiController]
    public class WorkoutsController : ControllerBase
    {
        private readonly IWorkoutRepository _repository;
        public WorkoutsController(IWorkoutRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets a list of all workouts with a list of sets containing the exercise name and id
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /workouts
        ///
        /// </remarks>
        /// <returns>A list of workouts. And each list displays the sets with the exercise name</returns>
        /// <response code="200">Returns a list of workouts</response>
        /// <response code="404">Could not find any workouts</response>
        [Authorize(Roles = Role.User)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOs.Workout.WithSets>>> GetAllAsync()
        {
            var workouts = await _repository.GetAllAsync();
            if(workouts == null)
            {
                return NotFound("Could not find any workouts");
            }
            return Ok(workouts);
        }

        /// <summary>
        /// Gets a specific workout based on the id with relevant information about the sets and exercises
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /workouts/:id
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A workout based on an id with relevant information</returns>
        /// <response code="200">Return a specific workout</response>
        /// <response code="404">Not found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.Workout.WithFullExercise>> GetByIdAsync(int id)
        {
            var workout = await _repository.GetByIdAsync(id);

            if (workout == null)
            {
                return NotFound();
            }

            return workout;
        }

        /// <summary>
        /// Adds a new Workout with sets and categories to the database
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     POST /workouts
        ///     {
        ///        "categoryIds": [
        ///             0
        ///        ],
        ///        "sets": [
        ///            {
        ///                 "exerciseId": 0,
        ///                 "exerciseRepetitions": 0
        ///             }
        ///         ],
        ///         "name": "string"
        ///     }
        /// </remarks>
        /// <param name="workout">an object with the name of the workout with sets of exercises, and a list of categories</param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">Bad request</response>
        [Authorize(Roles = Role.Contributer)]
        [HttpPost]
        public async Task<IActionResult> Post(DTOs.Workout.Post workout)
        {
            var isPosted = await _repository.AddAsync(workout, User);

            if(!isPosted) { return BadRequest("Could not add the workout"); }

            return NoContent();
        }

        /// <summary>
        /// Updates an attribute in a workout
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// [
        ///   { "op": "replace", "path": "/Name", "value": "Push-ups" }
        /// ]
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="patch">A JsonPatchDocument</param>
        /// <returns>No content</returns>
        /// <response code="200">No content</response>
        /// <response code="400">Bad request. Either wrong id, or bad request operation</response>
        /// <response code="401">Unathorized user</response>
        [Authorize(Roles = Role.Contributer)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody]JsonPatchDocument patch)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.PatchAsync(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            
            if (!isUpdated)
            {
                return BadRequest("Bad request. Could not find the workout, or the operation was trying to update unavailable objects");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a workout
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     DELETE workouts/:id
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="404">Could not find the workout</response>
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var isDeleted = await _repository.DeleteAsync(id);

            if (isDeleted == false)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
