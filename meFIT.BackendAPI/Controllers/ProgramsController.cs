﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.DTOs.Program;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/programs")]
    [ApiController]
    public class ProgramsController : ControllerBase
    {
        private readonly IProgramRepository _repository;

        public ProgramsController(IProgramRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets a list of all Programs in the database in an alphabetic order
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /programs
        ///
        /// </remarks>
        /// <returns>A list of programs</returns>
        /// <response code="200">Returns a list of programs</response>
        /// <response code="404">Could not find any programs</response>
        [Authorize(Roles = Role.User)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOs.Program.WithWorkout>>> GetAllProgramsAsync()
        {
            var programs = await _repository.GetProgramsSortedAsync();
            if (programs == null)
            {
                return NotFound("No programs found");
            }
            return Ok(programs);
        }

        /// <summary>
        /// Gets a specific Program based on the id with info about categories and workouts 
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /programs/:id
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A program with info about categories and workouts based on an id</returns>
        /// <response code="200">Return a specific program with info about categories and workouts</response>
        /// <response code="404">Not found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.Program.WithWorkout>> GetByIdAsync(int id)
        {
            var program = await _repository.GetProgramByIDAsync(id);

            if (program == null)
            {
                return NotFound("No exercise found with the given id");
            }

            return Ok(program);
        }

        /// <summary>
        /// Adds a new Program to the database, and also adds linkingtables to categories and workouts connected to the program
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     POST /programs
        ///     {
        ///         "categoryIds": [
        ///             0
        ///         ],
        ///         "programWorkouts": [
        ///             {
        ///                 "daysFromStart": 0,
        ///                 "workoutId": 0
        ///             }
        ///         ],
        ///         "name": "string"
        ///     }
        /// </remarks>
        /// <param name="program">an object with name, a list of categories and a list with objects containing workoutid and daysfromstart</param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="404">Bad request</response>
        [Authorize(Roles = Role.Contributer)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]DTOs.Program.Post program)
        {
            var isPosted = await _repository.InsertProgramAsync(program, User);

            if (!isPosted) { return BadRequest("Could not add program"); }
            
            return NoContent();
        }

        /// <summary>
        /// Updates the program
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// [
        ///   { "op": "replace", "path": "/Name", "value": "Push-ups" }
        /// ]
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="programPatch"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">Bad request. Either wrong id, or bad request operation</response>
        /// <response code="401">Unathorized user</response>
        [Authorize(Roles = Role.Contributer)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody]JsonPatchDocument programPatch)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.PatchAsync(id, programPatch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (!isUpdated)
            {
                return BadRequest("Bad request. Could not find the program, or the operation was trying to update unavailable objects");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a program
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     DELETE programs/:id
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="404">Could not find the program</response>
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var isDeleted = await _repository.DeleteAsync(id);
            if (isDeleted == false)
            {
                return NotFound();
            }
            
            return NoContent();
        }
    }
}
