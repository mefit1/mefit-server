﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/profile")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly IProfileRepository _repository;

        public ProfilesController(IProfileRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets a specific profile based on the id with relevant information about address, user, gender, and goal
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /profile/:id
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A profile with information about address, user, gender, and goal</returns>
        /// <response code="200">Return a specific profile</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="404">Not found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.Profile.WithAllInformation>> GetByIdAsync(int id)
        {
            DTOs.Profile.WithAllInformation profile;

            try
            {
                profile = await _repository.GetByIdAsync(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (profile == null)
            {
                return NotFound();
            }
            return Ok(profile);
        }

        /// <summary>
        /// Updates the profile
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// [
        ///   { "op": "replace", "path": "/Weight", "value": 70 }
        /// ]
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="patch">A JsonPatchDocument</param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="400">Bad request. Either wrong id, or bad request operation</response>
        [Authorize(Roles = Role.User)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAsync(int id, JsonPatchDocument patch)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.PatchAsync(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if(!isUpdated) { return BadRequest("Could not update the profile"); }

            return NoContent();
        }

        ///// <summary>
        ///// Adds a new profile to the database with an user and an address. the post takes a user dto and 
        ///// address dto with the profile parameters
        ///// </summary>
        ///// <remarks>
        ///// Sample request:
        /////     POST /profile
        /////     {
        ///// "genderId": 0,
        ///// "address": {
        /////   "addressLine1": "string",
        /////   "addressLine2": "string",
        /////   "addressLine3": "string",
        /////   "postalCode": "string",
        /////   "city": "string",
        /////   "country": "string"
        /////  },
        ///// "user": {
        /////   "firstName": "string",
        /////   "lastName": "string",
        /////   "dob": "2020-10-19T07:28:10.694Z",
        /////    "email": "string"
        ///// },
        ///// "weight": 0,
        ///// "height": 0,
        ///// "medicalConditions": "string",
        ///// "disabilities": "string"
        ///// }
        ///// </remarks>
        ///// <param name="profile">an base object containing weight, height, medicalConditions, disabilites, an user dto, and address dto</param>
        ///// <returns>No content</returns>
        ///// <response code="204">No content</response>
        ///// <response code="400">Bad request</response>
        //[Authorize(Roles = Role.User)]
        //[HttpPost]
        //public async Task<IActionResult> PostAsync([FromBody]DTOs.Profile.Post profile)
        //{
        //    var isPosted =  await _repository.AddAsync(profile, User);

        //    if(!isPosted) { return BadRequest("Could not post the profile"); }

        //    return NoContent();
        //}

        /// <summary>
        /// Deletes a profile
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     DELETE profile/:id
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="404">Could not find the workout</response>
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool isDeleted;

            try
            {
                isDeleted = await _repository.DeleteAsync(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            
            if (!isDeleted) { return NotFound("Could not find the profile you wanted to delete"); }

            return NoContent();
        }
    }
}
