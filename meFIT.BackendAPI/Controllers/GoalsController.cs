﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/goals")]
    [ApiController]
    public class GoalsController : ControllerBase
    {
        private readonly IGoalRepository _repository;

        public GoalsController(IGoalRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all goals related to profile with profileId
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /goals/byprofileid/2
        /// </remarks>
        /// <param name="profileId"></param>
        /// <response code="200">All goals related to profile with profileId</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="404">No profile with that id found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("byprofileid/{profileId}")]
        public async Task<ActionResult<ICollection<DTOs.Goal.BaseWithId>>> GetAllByProfileIdAsync(int profileId)
        {
            ICollection<DTOs.Goal.BaseWithId> goals;

            try
            {
                goals = await _repository.GetAllByProfileIdAsync(profileId, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (goals == null)
            {
                return NotFound("No profile with that id found");
            }

            return Ok(goals);
        }

        /// <summary>
        /// Get goal by id including related program and workouts 
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /goals/2
        /// </remarks>
        /// <param name="id"></param>
        /// <response code="200">A Goal including all workouts related</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="404">Did not find a goal with that id</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.Goal.WithWorkouts>> GetByIdIncludingWorkoutsAsync(int id)
        {
            DTOs.Goal.WithWorkouts goal = null;

            try
            {
                goal = await _repository.GetByIdIncludingWorkoutsAsync(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                Unauthorized();
            }

            if (goal == null)
            {
                return NotFound("Did not find a goal with that id");
            }

            return Ok(goal);
        }

        /// <summary>
        /// Posts a goal for a profile including workouts and a program
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /goals
        ///     {
        ///               "profileId": 1,
        ///               "goalProgramId": null,
        ///               "goalWorkouts": [
        ///                 {
        ///                   "workoutId": 1,
        ///                   "daysFromStart": 12
        ///                 }
        ///               ],
        ///               "startDate": "2020-10-19T08:07:45.422Z"
        ///     }
        /// 
        /// </remarks>
        /// <param name="goal"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">Invalid data posted</response>
        /// <response code="400">Goal adding failed</response>
        [Authorize(Roles = Role.User)]
        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] DTOs.Goal.WithGoalWorkoutsPost goal)
        {
            if (goal == null)
            {
                return BadRequest("Invalid data posted");
            }

            bool isAdded;

            try
            {
                isAdded = await _repository.AddAsync(goal, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (isAdded)
            {
                return NoContent();
            }

            return BadRequest("Goal adding failed");
        }

        /// <summary>
        /// Creates a GoalProgram from a ProgramId for User with given id
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// POST /goals/newprogram/1
        /// {2}
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="programId"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="401">Not the id of this user</response>
        /// <response code="400">Program adding failed</response>
        [Authorize(Roles = Role.User)]
        [HttpPost("newprogram/{id}")]
        public async Task<ActionResult<DTOs.GoalProgram.WithGoalWorkouts>> PostGoalProgramAsync(int id, [FromBody] int programId)
        {
            DTOs.GoalProgram.WithGoalWorkouts goalProgram = null;

            try
            {
                goalProgram = await _repository.AddGoalProgramAsync(programId, id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (goalProgram == null)
            {
                return BadRequest("Program adding failed");
            }

            return Ok(goalProgram);
        }

        /// <summary>
        /// Deletes goal by id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /goals/2
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="404">Goal with id not found</response>
        [Authorize(Roles = Role.User)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            bool isDeleted;

            try
            {
                isDeleted = await _repository.DeleteAsync(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (isDeleted == false)
            {
                return NotFound("Goal with id not found");
            }

            return NoContent();
        }

        /// <summary>
        /// Patches a goal
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE /goals/2
        ///     PATCH /goalprogramworkouts/{id}
        ///     [
        ///         { "op": "replace", "path": "/IsCompleted", "value": true }
        ///     ]
        ///     
        ///     [
        ///         { "op": "replace", "path": "/GoalProgramId", "value": null }
        ///     ]
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="401">Unathorized user</response>
        /// <response code="404">Goal with id not found</response>
        [Authorize(Roles = Role.User)]
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchAsync(int id, JsonPatchDocument patch)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.PatchAsync(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (isUpdated == false)
            {
                return NotFound("Goal with id not found");
            }

            return NoContent();
        }
    }
}
