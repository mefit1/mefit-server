﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.DTOs.Program;
using meFIT.BackendAPI.DTOs.User;
using meFIT.BackendAPI.Exceptions;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/user")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repository;
        public UsersController(IUserRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Register new user with profile
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST users/register
        ///     {
        ///       "password": "okfpoke09i3049ioij%F",
        ///       "passwordConfirmation": "okfpoke09i3049ioij%F",
        ///       "profile": {
        ///         "genderId": 1,
        ///         "address": {
        ///           "addressLine1": "Portveien 2",
        ///           "addressLine2": "",
        ///           "addressLine3": "",
        ///           "postalCode": "0415",
        ///           "city": "Oslo",
        ///           "country": "Norway"
        ///         },
        ///         "weight": 80,
        ///         "height": 182,
        ///         "medicalConditions": "borreliose",
        ///         "disabilities": ""
        ///       },
        ///       "firstName": "Lars",
        ///       "lastName": "Monsen",
        ///       "dob": "1970-10-21",
        ///       "email": "lars@monsen.no"
        ///     }
        /// 
        /// </remarks>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="204">NoContent</response>
        /// <response code="400">Email already in use / invalid password / password confirmation does not match password / user not created</response>
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public async Task<ActionResult> RegisterAsync([FromBody] DTOs.User.Register user)
        {
            bool output;
            try
            {
                output = await _repository.RegisterAsync(user, token =>
                {
                    return $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}/api/user/confirmation/{token}";
                });
            }
            catch (EmailDuplicateException)
            {
                return BadRequest("Email already in use");
            }
            catch (InvalidPasswordException)
            {
                return BadRequest("Invalid password. Must contain at least " +
                    "8 characters, " +
                    "one lower case character, " +
                    "one upper case character, " +
                    "one digit and " +
                    "one special character");
            }
            catch (PasswordNotConfirmedException)
            {
                return BadRequest("Password confirmation does not match password");
            }

            if (output)
                return NoContent();

            return BadRequest("User not created");
        }

        /// <summary>
        /// Send new password to email
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /user/newpassword
        /// {"email"}
        /// </remarks>
        /// <param name="email"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="400">Invalid email or problems sending email</response>
        [AllowAnonymous]
        [Route("newpassword")]
        [HttpPost]
        public async Task<ActionResult> ForgotPasswordAsync([FromBody] string email)
        {
            bool hasSentNewPassword;

            try
            {
                //Send new password or a reset link
                hasSentNewPassword = await _repository.SendNewPasswordAsync(email);
            }
            catch(ArgumentNullException)
            {
                return BadRequest("Must specify an email");
            }
            catch (Exception)
            {
                return BadRequest("Problems sending email");
            }

            if (hasSentNewPassword)
                return NoContent();

            return BadRequest("Invalid email");
        }

        /// <summary>
        /// Confirm email by passing token
        /// </summary>
        /// <remarks>Endpoint is used through confirmation link sent by email.</remarks>
        /// <param name="token"></param>
        /// <returns>No content</returns>
        /// <response code="204"></response>
        /// <response code="400">Invalid token</response>
        [AllowAnonymous]
        [HttpGet("confirmation/{token}")]
        public async Task<ActionResult> ConfirmEmailAsync(string token)
        {
            bool isConfirmed = await _repository.ConfirmEmailAsync(token);

            if (isConfirmed)
                return Ok("Email is confirmed");

            return BadRequest("Invalid token");
        }

        /// <summary>
        /// Login with email and password
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST users/login
        ///     {
        ///       "email": "harald.rex@kongehuset.no",
        ///       "password": "jhhfkjgFJKOJFWJ8ujrikjil"
        ///     }
        /// 
        /// </remarks>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="200">{ token: string, role: string }</response>
        /// <response code="400">Unable to log in</response>
        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<DTOs.User.Output>> LoginAsync([FromBody] DTOs.User.Login user)
        {
            var output = await _repository.LoginAsync(user);

            if (output == null)
                return BadRequest("Unable to log in");

            return Ok(output);
        }

        /// <summary>
        /// Get a list of all users async. For admin only
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET: /user
        /// </remarks>
        /// <returns>a list of users</returns>
        /// <response code="200">With a list of users</response>
        /// <response code="404">Could not find any users</response>
        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOs.User.WithRights>>> GetAllUsersAsync()
        {
            var users = await _repository.GetAllAsync();
            
            if(users == null) { return NotFound("There were no users in the db"); }
            
            return Ok(users);
        }

        /// <summary>
        /// Gets user by id with profile
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /users/:id
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A user object with a profile</returns>
        /// <response code="200">With an user object with profile</response>
        /// <response code="404">User not found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.User.WithProfile>> GetByIdAsync(int id)
        {
            DTOs.User.WithProfile userModel;

            try
            {
                userModel = await _repository.GetByIdAsync(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (userModel == null)
            {
                return NotFound("User not found");
            }

            return Ok(userModel);
        }

        /// <summary>
        /// For users to get their user info without using the id. uses the identifier to find the id and redirects to the right path
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     GET /user/userpath
        /// </remarks>
        /// <returns>Redirects to the right get user enpoint</returns>
        /// <response code="302">Redirect the user to the get user endpoint with their id</response>
        /// <response code="404">Could not find any user for the one issuing the request</response>
        [Authorize(Roles = Helpers.Role.User)]
        [HttpGet("path")]
        public async Task<IActionResult> GetUserWithoutIdAsync()
        {
            var requestURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            var redirect = await _repository.GetUserURL(User, requestURL);

            if (redirect == null) { return NotFound("Could not find the user"); }

            return Redirect(redirect);
        }

        /// <summary>
        /// Updates the password of the user. Must send the old password and the new password
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     POST: user/2/password
        ///     {
        ///         "OldPassword": "qwerty123GH%",
        ///         "NewPassword": "P@ssw0rd"
        ///     }
        /// </remarks>
        /// <param name="id">Id of the user that updates the password</param>
        /// <param name="password">An object that contains the old and the new password</param>
        /// <returns>No content</returns>
        /// <response code="204">No content when password is updated</response>
        /// <response code="400">Could not update the password</response>
        [Authorize(Roles = Helpers.Role.User)]
        [HttpPost("{id}/password")]
        public async Task<IActionResult> UpdatePasswordAsync(int id, [FromBody]DTOs.User.UpdatePassword password)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.UpdatePassword(id, password, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if(isUpdated == false) { return BadRequest("Could not update the password"); }

            return NoContent();
        }
        
        /// <summary>
        /// Patch on the user. Cannot use this to update id, password, role or email
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// PATCH: /user/2
        /// [
        ///     {"value": "Arne, "path": "/FirstName", "op": "replace"}
        /// ]
        /// </remarks>
        /// <param name="id">id of the one to patch</param>
        /// <param name="patch">A JsonPatchDocument</param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">Bad request, could not update the user</response>
        [Authorize(Roles = Helpers.Role.User)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchUserAsync(int id, [FromBody]JsonPatchDocument patch)
        {
            bool isUpdated;

            try
            {
                isUpdated = await _repository.PatchUser(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            
            if(isUpdated == false) { return BadRequest("Could not update the user"); }

            return NoContent();
        }

        /// <summary>
        /// Deletes a user and all their data. Only the admin and the user owning the data could use this
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// DELETE: /user/2
        /// </remarks>
        /// <param name="id">id of the user to delete</param>
        /// <returns>No content</returns>
        /// <response code="204">No content, user was deleted</response>
        /// <response code="404">Not found, could not find the user to delete</response>
        [Authorize(Roles = Helpers.Role.User)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            bool isDeleted;

            try
            {
                isDeleted = await _repository.Delete(id, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
            
            if (isDeleted == false)
            {
                return NotFound("Could not find the user");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the role of a user. for admin only
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// PATCH: /user/2/role
        /// [
        /// { "value": "Admin", "path": "/Role", "op": "replace" }
        /// ]
        /// </remarks>
        /// <param name="id">id of the user to update the role for</param>
        /// <param name="role">A string with the role to update to</param>
        /// <returns>No content</returns>
        /// <response code="204">No content. role was updated</response>
        /// <response code="400">Could not update the role of the user</response>
        [HttpPatch("{id}/role")]
        [Authorize(Roles = Helpers.Role.Admin)]
        public async Task<IActionResult> UpdateRoleAsync(int id, [FromBody]DTOs.User.UpdateRole role)
        {
            var isUpdated = await _repository.UpdateRole(id, role, User);
            if (!isUpdated)
            {
                return BadRequest("Could not update the role");
            }

            return NoContent();
        }

        /// <summary>
        /// Update the entire user entity
        /// </summary>
        /// <remarks>
        /// Sample request: 
        /// PUT /user/5:
        /// {
        ///     "profile": {
        ///         "genderId": 1,
        ///         "address": {
        ///             "addressLine1": "string",
        ///             "addressLine2": "string",
        ///             "addressLine3": "string",
        ///             "postalCode": "string",
        ///             "city": "string",
        ///             "country": "string"
        ///         },
        ///         "weight": 0,
        ///         "height": 0,
        ///         "medicalConditions": "string",
        ///         "disabilities": "string"
        ///     },
        ///     "firstName": "string",
        ///     "lastName": "string",
        ///     "dob": "2020-10-26T13:05:08.515Z"
        /// }
        /// </remarks>
        /// <returns>No content</returns>
        /// <response code="204">No content, updated the values in the database</response>
        /// <response code="401">The user is not allowed access to this entity</response>
        /// <response code="400">Bad request, and could not updated the entity it tried to update</response>
        [HttpPut("{id}")]
        [Authorize(Roles = Helpers.Role.User)]
        public async Task<IActionResult> PutUserAsync(int id, [FromBody]DTOs.User.UpdateUser user)
        {
            var isUpdated = false;
            try
            {
                isUpdated = await _repository.PutUser(id, user, User);
            }
            catch(UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if(!isUpdated)
            {
                return BadRequest("Could not update the user");
            }

            return NoContent();
        }
    }
}
