﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using meFIT.BackendAPI.Repositories.Implementations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/exercises")]
    [ApiController]
    public class ExercisesController : ControllerBase
    {
        private readonly IExerciseRepository _repository;

        public ExercisesController(IExerciseRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Get all exercises including its categories sorted by concrete category
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     
        ///     GET /exercises
        /// 
        /// </remarks>
        /// <returns>A list of exercises including its categories</returns>
        /// <response code="404">No exercises found</response>
        /// <response code="200">Ok with the exercises</response>
        [Authorize(Roles = Role.User)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTOs.Exercise.WithCategories>>> GetAllSortedByConcreteCategoriesAsync()
        {
            var exercises = await _repository.GetAllAsync();

            if (exercises.Count() == 0)
            {
                return NotFound("No exercises found");
            }

            return Ok(exercises);
        }

        /// <summary>
        /// Get an exercise by id including its categories
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     
        ///     GET /exercises/:id
        /// 
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>An exercise including its categories</returns>
        /// <response code="404">No exercise found with given ID</response>
        /// <response code="200">Ok with the exercise found</response>
        [Authorize(Roles = Role.User)]
        [HttpGet("{id}")]
        public async Task<ActionResult<DTOs.Exercise.WithCategories>> GetByIdAsync(int id)
        {
            var exercise = await _repository.GetByIdAsync(id);

            if (exercise == null)
            {
                return NotFound("No exercise found with given ID");
            }

            return Ok(exercise);
        }

        ///// <summary>
        ///// Patches an Exercise.
        ///// </summary>
        ///// <remarks>
        ///// Sample request: 
        ///// <code>
        ///// 
        /////     PATCH /exercise/{id}
        /////     [
        /////         { "op": "replace", "path": "/Name", "value": "Squeeezeeee!" }
        /////     ]
        /////     
        ///// </code>
        ///// </remarks>
        ///// <param name="id"></param>
        ///// <param name="patch"></param>
        ///// <returns></returns>
        ///// <response code="404">No exercise with id found</response>
        //[HttpPatch("{id}")]
        //public async Task<IActionResult> PatchAsync(int id,
        //    [FromBody] JsonPatchDocument<Exercise> patch)
        //{
        //    var isPatched = await _repository.PatchAsync(id, patch);

        //    if (isPatched)
        //    {
        //        return Ok();
        //    }

        //    return NotFound("No exercise with id found");
        //}

        /// <summary>
        /// Post an exercise with a list of category ids
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST /exercises
        ///     {
        ///         "targetMuscleGroups": [
        ///             1,2,3,4,5,6,7,8,9,10
        ///         ],
        ///         "name": "Mosh pit",
        ///         "description": "Act like a maniac (not THE Maniac)!",
        ///         "image": "",
        ///         "video": ""
        ///     }
        ///     
        /// </remarks>
        /// <param name="exercise"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">Invalid data posted</response>
        /// <response code="400">Exercise adding failed</response>
        [Authorize(Roles = Role.Contributer)]
        [HttpPost]
        public async Task<ActionResult> PostAsync([FromBody] DTOs.Exercise.WithCategoriesPost exercise)
        {
            if (exercise == null)
            {
                return BadRequest("Invalid data posted");
            }

            var isPosted = await _repository.AddAsync(exercise, User);

            if (isPosted)
            {
                return NoContent();
            }

            return BadRequest("Exercise adding failed");
        }

        /// <summary>
        /// Delete an exercise by id and its related sets and exercise categories
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">No content</response>
        /// <response code="400">Exercise deletion failed</response>
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var isDeleted = await _repository.DeleteAsync(id);
            if (isDeleted)
            {
                return NoContent();
            }

            return BadRequest("Exercise deletion failed");
        }
    }
}
