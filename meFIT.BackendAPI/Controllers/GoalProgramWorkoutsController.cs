﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/goalprogramworkouts")]
    [ApiController]
    public class GoalProgramWorkoutsController : ControllerBase
    {
        private readonly IGoalProgramWorkoutRepository _repository;

        public GoalProgramWorkoutsController(IGoalProgramWorkoutRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Change value of IsCompleted OR DaysFromStart in GoalProgramWorkout with id
        /// </summary>
        /// <remarks>
        /// Sample requests:
        /// 
        ///     PATCH /goalprogramworkouts/{id}
        ///     [
        ///         { "op": "replace", "path": "/IsCompleted", "value": true }
        ///     ]
        ///     
        ///     PATCH /goalprogramworkouts/{id}
        ///     [
        ///         { "op": "replace", "path": "/DaysFromStart", "value": 7 }
        ///     ]
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">GoalProgramWorkout did not update</response>
        /// <response code="401">Unathorized user</response>
        [Authorize(Roles = Role.User)]
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchAsync(int id,
            [FromBody] JsonPatchDocument patch)
        {
            bool IsPatched;

            try
            {
                IsPatched = await _repository.PatchAsync(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (IsPatched)
            {
                return NoContent();
            }

            return BadRequest("GoalProgramWorkout did not update");
        }
    }
}
