﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _repository;

        public CategoriesController(ICategoryRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets a list of all categories
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     GET /categories
        /// 
        /// </remarks>
        /// <response code="404">No categories found</response>
        /// <response code="200">Ok with the categories</response>
        /// <returns>A list of categories</returns>
        [Authorize(Roles = Role.User)]
        [HttpGet]
        public async Task<ActionResult<ICollection<Category>>> GetAllAsync()
        {
            var categories = await _repository.GetAllAsync();

            if (categories == null)
            {
                NotFound("No categories found");
            }

            return Ok(categories);
        }
    }
}
