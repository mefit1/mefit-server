﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/goalworkouts")]
    [ApiController]
    public class GoalWorkoutsController : ControllerBase
    {
        private readonly IGoalWorkoutRepository _repository;

        public GoalWorkoutsController(IGoalWorkoutRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Change value of IsCompleted OR DaysFromStart in GoalWorkout with id
        /// </summary>
        /// <remarks>
        /// Sample requests:
        /// 
        ///     PATCH /goalworkouts/{id}
        ///     [
        ///         { "op": "replace", "path": "/IsCompleted", "value": true }
        ///     ]
        ///     
        ///     PATCH /goalworkouts/{id}
        ///     [
        ///         { "op": "replace", "path": "/DaysFromStart", "value": 7 }
        ///     ]
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns>No content</returns>
        /// <response code="204">No content</response>
        /// <response code="400">GoalWorkout did not update</response>
        /// <response code="401">Unathorized user</response>
        [Authorize(Roles = Role.User)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchAsync(int id,
            [FromBody] JsonPatchDocument patch)
        {
            bool isPatched;

            try
            {
                isPatched = await _repository.PatchAsync(id, patch, User);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if (isPatched)
            {
                return NoContent();
            }

            return BadRequest("GoalWorkout did not update");
        }

        /// <summary>
        /// Adds a new goalworkout to the goal.
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// POST /goalworkouts/
        /// {
        ///     "goalId": 1,
        ///     "workoutId": 1,
        ///     "daysFromStart": 5
        /// }
        /// </remarks>
        /// <param name="goalWorkout"></param>
        /// <returns>No content</returns>
        /// <response code="401">Cannot post a goalworkout to a goal that is not owned by you</response>
        /// <response code="400">Could not add the goalworkout to the database</response>
        /// <response code="200">Successfully added the goalworkout to the database, and the id is returned</response>
        [Authorize(Roles = Role.User)]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] DTOs.GoalWorkout.Post goalWorkout)
        {
            int isPosted;
            try
            {
                isPosted = await _repository.AddAsync(goalWorkout, User);
            }
            catch(UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if(isPosted == 0) { return BadRequest("Could not post the goalworkout"); }

            return Ok(new { Id = isPosted});
        }

        /// <summary>
        /// Deletes a goalworkout
        /// </summary>
        /// <param name="id">Id of the goalworkout to delete</param>
        /// <remarks>
        /// Sample request:
        /// DELETE /goalworkouts/:id
        /// </remarks>
        /// <returns>No content</returns>
        /// <response code="401">Cannot delete an entry that is not owned by you</response>
        /// <response code="404">Could not find the entry to delete</response>
        /// <response code="204">Successfully deleted the entry</response>
        [Authorize(Roles = Role.User)]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            bool isDeleted;

            try
            {
                isDeleted = await _repository.DeleteAsync(id, User);
            }
            catch(UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            if(!isDeleted) { return NotFound("Could not find the user to delete"); }

            return NoContent();
        }
    }
}
