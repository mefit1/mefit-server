﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace meFIT.BackendAPI.Controllers
{
    [Authorize]
    [Route("api/genders")]
    [ApiController]
    public class GendersController : ControllerBase
    {
        private readonly IGenderRepository _repository;

        public GendersController(IGenderRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets a list of genders
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///     
        ///     GET /genders
        /// 
        /// </remarks>
        /// <response code="404">No genders found</response>
        /// <response code="200">Ok, with list of genders</response>
        /// <returns>A list of genders</returns>
        [Authorize(Roles = Role.User)]
        [HttpGet]
        public async Task<ActionResult<ICollection<Gender>>> GetAllAsync()
        {
            var genders = await _repository.GetAllAsync();

            if (genders == null)
            {
                return NotFound("No genders found");
            }

            return Ok(genders);
        }
    }
}
