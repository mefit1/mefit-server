﻿using meFIT.BackendAPI.DTOs.Goal;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IGoalRepository
    {
        Task<DTOs.Goal.WithWorkouts> GetByIdIncludingWorkoutsAsync(int id, ClaimsPrincipal user);
        Task<ICollection<DTOs.Goal.BaseWithId>> GetAllByProfileIdAsync(int profileId, ClaimsPrincipal user);
        Task<bool> AddAsync(DTOs.Goal.WithGoalWorkoutsPost goal, ClaimsPrincipal user);
        Task<bool> DeleteAsync(int id, ClaimsPrincipal user);
        Task<DTOs.GoalProgram.WithGoalWorkouts> AddGoalProgramAsync(int programId, int id, ClaimsPrincipal user);
        Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user);
    }
}
