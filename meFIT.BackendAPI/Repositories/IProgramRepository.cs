﻿using meFIT.BackendAPI.DTOs.Program;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IProgramRepository
    {
        Task<IEnumerable<DTOs.Program.WithWorkout>> GetProgramsSortedAsync();
        Task<bool> InsertProgramAsync(DTOs.Program.Post program, ClaimsPrincipal user);
        Task<DTOs.Program.WithWorkout> GetProgramByIDAsync(int programId);
        Task<bool> DeleteAsync(int programId);
        Task<bool> PatchAsync(int id, JsonPatchDocument patchProgram, ClaimsPrincipal user);
    }
}
