﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.DTOs.Program;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class ProgramRepository : IProgramRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public ProgramRepository(AppDbContext context, IAuthorizationService auth)
        {
            _context = context;
            _auth = auth;
        }

        public async Task<Models.Program> GetById(int id)
        {
            return await _context.Program
                .FindAsync(id);
        }

        //Gets all the programs in the db sorted by the name
        public async Task<IEnumerable<DTOs.Program.WithWorkout>> GetProgramsSortedAsync()
        {
            var programs = await _context.Program
                .Include(p => p.ProgramCategories)
                .ThenInclude(pc => pc.Category)
                .Include(p => p.ProgramWorkouts)
                .ThenInclude(pw => pw.Workout)
                .OrderBy(p => p.Name)
                .Select(p => new DTOs.Program.WithWorkout(p) { })
                .ToListAsync();

            //Return empty list if there are no programs in the db
            if (programs.Count().Equals(0))
            {
                return null;
            }
            
            return programs;
        }

        public async Task<DTOs.Program.WithWorkout> GetProgramByIDAsync(int programId)
        {
            var program = await _context.Program
                .Where(p => p.Id == programId)
                .Include(p => p.ProgramWorkouts)
                .ThenInclude(pw => pw.Workout)
                .Include(p => p.ProgramCategories)
                .ThenInclude(pc => pc.Category)
                .Select(p => new DTOs.Program.WithWorkout(p) { })
                .FirstOrDefaultAsync();

            return program;
        }

        public async Task<bool> DeleteAsync(int programId)
        {
            var program = await _context.Program.FindAsync(programId);
            if (program == null)
            {
                return false;
            }
            _context.Program.Remove(program);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> InsertProgramAsync(DTOs.Program.Post programDTO, ClaimsPrincipal user)
        {
            //When adding a program it also adds linkingtables to categories and workouts
            var program = new Models.Program(programDTO);

            if (program == null)
                return false;

            program.Owner = user.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Email).Value;

            var programEntry = await _context.Program.AddAsync(program);

            if(programEntry == null) { return false; }

            await _context.SaveChangesAsync();
            return true;
        }
        
        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var program = await GetById(id);
            if (program == null)
            {
                return false;
            };

            var authResult = await _auth.AuthorizeAsync(user, program, "OwnerPolicy");

            if (!authResult.Succeeded)
                throw new UnauthorizedAccessException();

            //Check the operations in the patch. cannot update id
            foreach (var operation in patch.Operations)
            {
                if(operation.path == "/Id" )
                {
                    return false;
                }
            }

            patch.ApplyTo(program);
            await _context.SaveChangesAsync();  
            return true;
        }
    }
}
