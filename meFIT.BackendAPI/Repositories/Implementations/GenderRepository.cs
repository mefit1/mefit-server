﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class GenderRepository : IGenderRepository
    {
        private readonly AppDbContext _context;

        public GenderRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Gender>> GetAllAsync()
        {
            var genders = await _context.Gender.ToListAsync();

            //Return empty list if there are no genders in the db
            if (genders.Count().Equals(0))
            {
                return null;
            }

            return genders;
        }
    }
}
