﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _context;

        public CategoryRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Category>> GetAllAsync()
        {
            var categories = await _context.Category.ToListAsync();

            if (categories.Count() == 0)
            {
                return null;
            }

            return categories;
        }
    }
}
