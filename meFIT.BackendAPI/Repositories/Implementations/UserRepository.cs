﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Exceptions;
using meFIT.BackendAPI.Helpers;
using meFIT.BackendAPI.Models;
using meFIT.BackendAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Server.IIS.Core;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;
        private readonly AppSettings _appSettings;
        private readonly EmailSettings _emailSettings;
        private readonly IAuthorizationService _auth;
        private readonly IPasswordHasher<User> _hasher;
        private readonly IEmailService _email;

        public UserRepository(AppDbContext context, 
            IOptions<AppSettings> appSettings, 
            IOptions<EmailSettings> emailSettings,
            IAuthorizationService auth, 
            IPasswordHasher<User> hasher,
            IEmailService email)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _emailSettings = emailSettings.Value;
            _auth = auth;
            _hasher = hasher;
            _email = email;
        }

        public async Task<bool> Delete(int id, ClaimsPrincipal user)
        {
            var userModel = await _context.User
                .Where(u => u.Id == id)
                .Include(u => u.Profile)
                .ThenInclude(p => p.Address)
                .Include(u => u.Profile)
                .ThenInclude(p => p.Goals)
                .ThenInclude(g => g.GoalWorkouts)
                .Include(u => u.Profile)
                .ThenInclude(p => p.Goals)
                .ThenInclude(g => g.GoalProgram)
                .ThenInclude(gp => gp.GoalProgramWorkouts)
                .FirstOrDefaultAsync();

            if (userModel == null)
            {
                return false;
            }

            // Admin and Owner can delete user
            var authResult = await _auth.AuthorizeAsync(user, userModel, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }
            
            //The address belongs to the profile so it needs to be deleted
            if (userModel.Profile.Address != null)
            {
                _context.Remove(userModel.Profile.Address);
            }

            //Removes all the goals with goalprogram and goalworkout, as they belong to the deleted user.
            foreach (var goal in userModel.Profile.Goals)
            {
                if (goal.GoalProgram != null)
                {
                    _context.RemoveRange(goal.GoalProgram.GoalProgramWorkouts);
                    _context.Remove(goal.GoalProgram);
                }
                if (!goal.GoalWorkouts.Count().Equals(0))
                {
                    _context.RemoveRange(goal.GoalWorkouts);
                }
                _context.Remove(goal);
            }
            //Removes a user and profile finally
            _context.Profile.Remove(userModel.Profile);
            _context.User.Remove(userModel);
            
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<DTOs.User.WithProfile> GetByIdAsync(int id, ClaimsPrincipal user)
        {
            var userModel = await _context.User
                .Where(u => u.Id == id)
                .Include(u => u.Profile)
                .ThenInclude(p => p.Address)
                .Include(u => u.Profile)
                .ThenInclude(p => p.Gender)
                .SingleOrDefaultAsync();

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, userModel, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            if(userModel == null)
            {
                return null;
            }

            var userWithoutPassword = new DTOs.User.WithProfile(userModel);

            return userWithoutPassword;
        }

        public async Task<bool> RegisterAsync(DTOs.User.Register userDTO, Func<string, string> redirect)
        {
            // Mapping user
            var user = new User(userDTO);

            // Checking if Email already in use
            var userExist = await _context.User.AnyAsync(u => u.Email == user.Email);
            if (userExist)
                throw new EmailDuplicateException();

            // Validating Password
            if (!Validate.Password(user.Password))
                throw new InvalidPasswordException();

            // Confiming Password
            if (user.Password != userDTO.PasswordConfirmation)
                throw new PasswordNotConfirmedException();

            // Hashing Password
            user.Password = _hasher.HashPassword(user, user.Password);

            // Adding user
            var userEntry = await _context.User.AddAsync(user);
            if (userEntry.Entity == null)
                return false;

            // Adding profile for user
            var profile = new Profile(userDTO.Profile);
            profile.User = user;
            var profileEntry = await _context.Profile.AddAsync(profile);
            if (profileEntry.Entity == null)
                return false;

            await _context.SaveChangesAsync();

            var token = GetConfirmationToken(user);

            //Send email with token
            string confirmationLink = redirect(token);
            _email.Send(_emailSettings.SmtpUser, user.Email, 
                "Confirm Email for meFit-account", 
                $"Please confirm your email by visiting <a href='{ confirmationLink }'>this confirmation link.</a>");

            return true;
        }

        public async Task<DTOs.User.Output> LoginAsync(DTOs.User.Login credentials)
        {
            var user = await _context.User
                .SingleOrDefaultAsync(u => u.Email == credentials.Email);

            // VerifyHashedPassword
            if (_hasher.VerifyHashedPassword(user, user.Password, credentials.Password) == PasswordVerificationResult.Failed)
                return null;

            if (user == null)
                return null;

            if (!user.IsConfirmed)
                return null;

            var token = GetLoginToken(user);

            return new DTOs.User.Output { Token = token, Role = user.Role };
        }

        private string GetConfirmationToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, user.Email)
                }),
                Expires = DateTime.UtcNow.AddDays(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            if (tokenHandler.CanWriteToken)
                return tokenHandler.WriteToken(token);

            return string.Empty;
        }

        private string GetLoginToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            if (tokenHandler.CanWriteToken)
                return tokenHandler.WriteToken(token);

            return string.Empty;
        }

        public async Task<string> GetUserURL(ClaimsPrincipal user, string requestURL)
        {
            var userEmail = user.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Email).Value;

            var userModel = await _context.User.SingleOrDefaultAsync(u => u.Email == userEmail);

            if (userModel == null) { return null; }
            
            var response = requestURL + "/api/user/" + userModel.Id;

            return response;
        }

        public async Task<bool> PatchUser(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var userModel = await _context.User
                .Include(u => u.Profile)
                .ThenInclude(p => p.Address)
                .SingleOrDefaultAsync(p => p.Id == id);

            if (userModel == null) { return false; }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, userModel, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Cannot update the id or password in a patch. There is only admin that can update the role path
            foreach (var operation in patch.Operations)
            {
                if (operation.path == "/Id" || operation.path == "/Password" || operation.path == "/Role" || operation.path == "/Email")
                {
                    return false;
                }
            }

            patch.ApplyTo(userModel);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdatePassword(int id, DTOs.User.UpdatePassword password, ClaimsPrincipal user)
        {
            var userModel = await _context.User.FindAsync(id);

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, userModel, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            if (_hasher.VerifyHashedPassword(userModel, userModel.Password, password.OldPassword) == PasswordVerificationResult.Failed) { return false; }

            userModel.Password = _hasher.HashPassword(userModel, password.NewPassword);

            _context.User.Update(userModel);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateRole(int id, DTOs.User.UpdateRole role, ClaimsPrincipal user)
        {
            var userModel = await _context.User.FindAsync(id);

            if(userModel == null) { return false; }

            //Set the role
            switch(role.Role)
            {
                case "User":
                    userModel.Role = Helpers.Role.User;
                    break;
                case "Contributor":
                    userModel.Role = Helpers.Role.Contributer;
                    break;
                case "Admin":
                    userModel.Role = Helpers.Role.Admin;
                    break;
                default:
                    return false;
            }

            _context.User.Update(userModel);

            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<DTOs.User.WithRights>> GetAllAsync()
        {
            var users = await _context.User
                .Select(u => new DTOs.User.WithRights(u))
                .ToListAsync();

            return users;
        }

        public async Task<bool> ConfirmEmailAsync(string token)
        {
            var email = ExtractEmailFromToken(token);

            if (email == string.Empty)
                return false;

            var user = await _context.User.SingleOrDefaultAsync(u => u.Email == email);

            if (user == null)
                return false;

            user.IsConfirmed = true;
            await _context.SaveChangesAsync();

            return true;
        }

        private string ExtractEmailFromToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(_appSettings.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                return jwtToken.Claims.SingleOrDefault(x => x.Type == "email").Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public async Task<bool> SendNewPasswordAsync(string email)
        {
            if(string.IsNullOrWhiteSpace(email)) { throw new ArgumentNullException(); }
            email = email.ToLower();

            var user = await _context.User.SingleOrDefaultAsync(u => u.Email.ToLower() == email);

            if (user == null)
                return false;

            var newPassword = PasswordGenerator.GenerateRandom();
            user.Password = _hasher.HashPassword(user, newPassword);

            //Send email with password
            _email.Send(_emailSettings.SmtpUser, user.Email,
                "New password for meFit-account",
                $"Here is your new password for your meFit-account: {newPassword}");

            await _context.SaveChangesAsync();

            return true;
        }
        public async Task<bool> PutUser(int id, DTOs.User.UpdateUser userDTO, ClaimsPrincipal user)
        {
            var userModel = await _context.User
                .Include(u => u.Profile)
                .ThenInclude(p => p.Address)
                .SingleOrDefaultAsync(u => u.Id == id);
            
            if(userModel == null) { return false; }

            var authResult = await _auth.AuthorizeAsync(user, userModel, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Update the model with values from dto
            userDTO.Update(userModel);

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
