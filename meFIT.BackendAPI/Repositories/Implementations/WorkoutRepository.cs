﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class WorkoutRepository : IWorkoutRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public WorkoutRepository(AppDbContext context, IAuthorizationService auth) 
        {
            _context = context;
            _auth = auth;
        }

        public async Task<bool> AddAsync(DTOs.Workout.Post workoutDTO, ClaimsPrincipal user)
        {
            //When adding a workout there are also created set tables and linking tables to categories
            var workout = new Models.Workout(workoutDTO);

            workout.Owner = user.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value;

            var workoutEntry = await _context.Workout.AddAsync(workout);

            if (workoutEntry == null) { return false; }

            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var workout = await _context.Workout.FindAsync(id);
            if (workout == null)
            {
                return false;
            }
            _context.Workout.Remove(workout);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<DTOs.Workout.IWithSets>> GetAllAsync()
        {
            var workouts = await _context.Workout
                .Include(w => w.Sets)
                .ThenInclude(s => s.Exercise)
                .ThenInclude(e => e.TargetMuscleGroups)
                .ThenInclude(tmg => tmg.Category)
                .Include(w => w.WorkoutCategories)
                .ThenInclude(wc => wc.Category)
                .Select(w => new DTOs.Workout.WithSets(w))
                .ToListAsync();

            //Return empty list if there are no programs in the db
            if (workouts.Count().Equals(0))
            {
                return null;
            }

            return workouts;
        }

        public async Task<DTOs.Workout.WithFullExercise> GetByIdAsync(int id)
        {
            var workout = await _context.Workout
                .Where(w => w.Id == id)
                .Include(w => w.Sets)
                .ThenInclude(s => s.Exercise)
                .ThenInclude(e => e.TargetMuscleGroups)
                .ThenInclude(tmg => tmg.Category)
                .Include(w => w.WorkoutCategories)
                .ThenInclude(wc => wc.Category)
                .Select(w => new DTOs.Workout.WithFullExercise(w))
                .FirstOrDefaultAsync();

            return workout;
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var workout = await GetModelAsync(id);
            if (workout == null)
            {
                return false;
            };

            var authResult = await _auth.AuthorizeAsync(user, workout, "OwnerPolicy");

            if (!authResult.Succeeded)
                throw new UnauthorizedAccessException();

            //Cannot update the id
            foreach (var operation in patch.Operations)
            {
                if (operation.path == "/Id")
                {
                    return false;
                }
            }

            patch.ApplyTo(workout);
            await _context.SaveChangesAsync();
            return true;
        }

        private async Task<Models.Workout> GetModelAsync(int id)
        {
            return await _context.Workout.FindAsync(id);
        }
    }
}
