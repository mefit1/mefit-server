﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.DTOs.Exercise;
using meFIT.BackendAPI.DTOs.Goal;
using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class GoalRepository : IGoalRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public GoalRepository(AppDbContext context, IAuthorizationService auth)
        {
            _context = context;
            _auth = auth;
        }

        public async Task<ICollection<BaseWithId>> GetAllByProfileIdAsync(int profileId, ClaimsPrincipal user)
        {
            var goals = await _context.Goal.Where(g => g.ProfileId == profileId)
                                           .Select(g => new DTOs.Goal.BaseWithId(g))
                                           .ToListAsync();


            var profile = await _context.Profile
                                        .Include(p => p.User)
                                        .SingleOrDefaultAsync(p => p.Id == profileId);

            if (goals == null)
            {
                return null;
            }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            return goals;
        }

        public async Task<bool> AddAsync(DTOs.Goal.WithGoalWorkoutsPost goalDTO, ClaimsPrincipal user)
        {
            var goal = new Goal(goalDTO);

            if (goal == null)
            {
                return false;
            }

            //Checking if profile in goal is owned by user
            var profile = await _context.Profile
                .Include(p => p.User)
                .SingleOrDefaultAsync(g => g.Id == goal.ProfileId);

            var authResult = await _auth.AuthorizeAsync(user, profile.User, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException("User is not the owner of profileId"); }

            goal.Owner = user.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value;
            goal.GoalWorkouts?.Select(gw => gw.Owner = goal.Owner);

            var entry = await _context.Goal.AddAsync(goal);

            //TODO: Does the profile and workouts exist?

            if (entry.Entity == null)
            {
                return false;
            }

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id, ClaimsPrincipal user)
        {
            var goal = await _context.Goal
                                     .Include(g => g.Profile)
                                        .ThenInclude(p => p.User)
                                     .SingleOrDefaultAsync(g => g.Id == id);

            if (goal == null)
            {
                return false;
            }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, goal.Profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            var goalProgram = await _context.GoalProgram.FindAsync(goal.GoalProgramId);

            if (goalProgram != null)
            {
                // Delete GoalProgramWorkouts
                if (goalProgram.GoalProgramWorkouts != null)
                    _context.GoalProgramWorkout.RemoveRange(goalProgram.GoalProgramWorkouts);

                // Delete GoalProgramCategories
                if (goalProgram.GoalProgramCategories != null)
                    _context.GoalProgramCategory.RemoveRange(goalProgram.GoalProgramCategories);

                // Delete GoalPrograms
                _context.GoalProgram.Remove(goalProgram);
            }

            // Delete GoalWorkouts
            if (goal.GoalWorkouts != null)
                _context.GoalWorkout.RemoveRange(goal.GoalWorkouts);

            _context.Goal.Remove(goal);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<DTOs.Goal.WithWorkouts> GetByIdIncludingWorkoutsAsync(int id, ClaimsPrincipal user)
        {
            var goal = await _context.Goal
                .Include(goal => goal.GoalProgram)
                    .ThenInclude(gp => gp.GoalProgramWorkouts)
                    .ThenInclude(gpw => gpw.Workout)
                    .ThenInclude(w => w.WorkoutCategories)
                    .ThenInclude(wc => wc.Category)
                .Include(goal => goal.GoalProgram)
                    .ThenInclude(gp => gp.GoalProgramCategories)
                    .ThenInclude(gpc => gpc.Category)
                .Include(goal => goal.GoalWorkouts)
                    .ThenInclude(gw => gw.Workout)
                    .ThenInclude(w => w.WorkoutCategories)
                    .ThenInclude(wc => wc.Category)
                .Include(goal => goal.Profile)
                    .ThenInclude(p => p.User)
                .FirstOrDefaultAsync(g => g.Id == id);

            if (goal == null)
            {
                return null;
            }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, goal.Profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            var goalDTO = new DTOs.Goal.WithWorkouts(goal);

            //Order by days from start
            goalDTO.GoalWorkouts = goalDTO.GoalWorkouts.OrderBy(wo => wo.DaysFromStart).ToList();

            if (goalDTO == null)
            {
                return null;
            }

            return goalDTO;
        }

        public async Task<DTOs.GoalProgram.WithGoalWorkouts> AddGoalProgramAsync(int programId, int id, ClaimsPrincipal user)
        {
            var program = await _context.Program
                .Include(p => p.ProgramWorkouts)
                .Include(p => p.ProgramCategories)
                .FirstOrDefaultAsync(p => p.Id == programId);

            var goal = await _context.Goal
                .Include(g => g.GoalProgram)
                    .ThenInclude(gp => gp.GoalProgramWorkouts)
                .Include(g => g.GoalProgram)
                    .ThenInclude(gp => gp.GoalProgramCategories)
                .Include(g => g.GoalWorkouts)
                .Include(g => g.Profile)
                    .ThenInclude(p => p.User)
                .FirstOrDefaultAsync(g => g.Id == id);

            if (program == null) return null;
            if (goal == null) return null;

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, goal.Profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            if(goal.GoalProgram != null)
            {
                // Cascade delete old GoalProgram
                _context.GoalProgramCategory.RemoveRange(goal.GoalProgram.GoalProgramCategories);
                _context.GoalProgramWorkout.RemoveRange(goal.GoalProgram.GoalProgramWorkouts);
                //_context.GoalProgram.Remove(goal.GoalProgram);
            }

            var goalProgram = new GoalProgram
            {
                Name = program.Name
            };

            goalProgram.GoalProgramWorkouts = new List<GoalProgramWorkout>();
            foreach (var programWorkout in program.ProgramWorkouts)
            {
                // create new GoalProgramWorkouts and add them to list
                goalProgram.GoalProgramWorkouts.Add(new GoalProgramWorkout
                {
                    DaysFromStart = programWorkout.DaysFromStart,
                    WorkoutId = programWorkout.WorkoutId,
                    Owner = goal.Profile.User.Email,
                    IsCompleted = false
                });
            }
            goalProgram.GoalProgramCategories = new List<GoalProgramCategory>();
            foreach (var programCategory in program.ProgramCategories)
            {
                // create new GoalProgramCategories and add them to list
                goalProgram.GoalProgramCategories.Add(new GoalProgramCategory
                {
                    CategoryId = programCategory.CategoryId
                });
            }

            // Add to DB
            _context.GoalProgram.Add(goalProgram);

            goal.GoalProgram = goalProgram;

            await _context.SaveChangesAsync();

            return new DTOs.GoalProgram.WithGoalWorkouts(goal.GoalProgram);
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var goal = await _context.Goal.FindAsync(id);

            if (goal == null)
            {
                return false;
            }

            var userModel = await _context.User.SingleOrDefaultAsync(u => u.Email == goal.Owner);
            var authResult = await _auth.AuthorizeAsync(user, userModel, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Make sure only one operation and that it's applied to IsCompleted OR DaysFromStart OR IsActive
            if (patch.Operations.Count() == 1 &&
                (patch.Operations[0].path == "/IsCompleted" ||
                patch.Operations[0].path == "/StartDate" ||
                patch.Operations[0].path == "/IsActive"))
            {
                patch.ApplyTo(goal);
                await _context.SaveChangesAsync();

                return true;

            //Patching GoalProgramId to -1 means removing GoalProgram
            } else if (patch.Operations.Count() == 1 &&
                      patch.Operations[0].path == "/GoalProgramId" &&
                      patch.Operations[0].value == null)
            {
                patch.ApplyTo(goal);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
