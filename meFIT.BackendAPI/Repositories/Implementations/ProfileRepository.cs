﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.DTOs.Profile;
using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public ProfileRepository(AppDbContext context, IAuthorizationService auth) 
        {
            _context = context;
            _auth = auth;
        }

        //public async Task<bool> AddAsync(DTOs.Profile.Post profileDto, ClaimsPrincipal user)
        //{
        //    //When posting a new profile, one also post an address and user to the db
        //    var profile = new Models.Profile(profileDto);

        //    if (profile == null)
        //        return false;

        //    profile.Owner = user.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Email).Value;

        //    var profileEntry = await _context.Profile.AddAsync(profile);
        //    if (profileEntry == null) { return false; }

        //    await _context.SaveChangesAsync();
        //    return true;
        //}

        public async Task<bool> DeleteAsync(int id, ClaimsPrincipal user)
        {
            var profile = await _context.Profile
                .Where(p => p.Id == id)
                .Include(p => p.Address)
                .Include(p => p.Goals)
                .ThenInclude(g => g.GoalWorkouts)
                .Include(p => p.Goals)
                .ThenInclude(g => g.GoalProgram)
                .ThenInclude(gp => gp.GoalProgramWorkouts)
                .Include(p => p.User)
                .FirstOrDefaultAsync();

            if (profile == null)
            {
                return false;
            }

            // Authorizing both admin and owner
            var authResult = await _auth.AuthorizeAsync(user, profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //The address belongs to the profile so it needs to be deleted
            if(profile.Address != null)
            {
                _context.Remove(profile.Address);
            }

            //Removes all the goals with goalprogram and goalworkout, as they belong to the deleted user.
            foreach(var goal in profile.Goals)
            {
                if (goal.GoalProgram != null) 
                {
                    _context.RemoveRange(goal.GoalProgram.GoalProgramWorkouts);
                    _context.Remove(goal.GoalProgram); 
                }
                if (!goal.GoalWorkouts.Count().Equals(0)) 
                {
                    _context.RemoveRange(goal.GoalWorkouts); 
                }
                _context.Remove(goal);
            }

            _context.Profile.Remove(profile);
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<DTOs.Profile.WithAllInformation> GetByIdAsync(int id, ClaimsPrincipal user)
        {
            var profile = await _context.Profile
                .Include(p => p.Gender)
                .Include(p => p.Goals)
                .Include(p => p.User)
                .Include(p => p.Address)
                .SingleOrDefaultAsync(p => p.Id == id);

            var profileDTO = new DTOs.Profile.WithAllInformation(profile);

            // Authorizing both admin and owner
            var authResult = await _auth.AuthorizeAsync(user, profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            return profileDTO;
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var profile = await _context.Profile
                                        .Include(p => p.User)
                                        .Include(p => p.Address)
                                        .SingleOrDefaultAsync(p => p.Id == id);
            if (profile == null)
            {
                return false;
            };

            // Authorizing both admin and owner
            var authResult = await _auth.AuthorizeAsync(user, profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Cannot update the id
            foreach (var operation in patch.Operations)
            {
                if (operation.path == "/Id" || operation.path == "/Owner")
                {
                    return false;
                }
            }

            patch.ApplyTo(profile);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
