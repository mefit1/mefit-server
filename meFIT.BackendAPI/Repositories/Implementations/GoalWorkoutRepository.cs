﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.DTOs.GoalWorkout;
using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class GoalWorkoutRepository : IGoalWorkoutRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public GoalWorkoutRepository(AppDbContext context, IAuthorizationService auth)
        {
            _context = context;
            _auth = auth;
        }

        public async Task<int> AddAsync(Post workout, ClaimsPrincipal user)
        {
            var gwModel = new GoalWorkout(workout);

            var goalWithUser = await _context.Goal
                .Include(g => g.Profile)
                .ThenInclude(p => p.User)
                .SingleOrDefaultAsync(gw => gw.Id == workout.GoalId);
            
            if(goalWithUser == null) { return 0; }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, goalWithUser.Profile.User, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            // Set owner
            gwModel.Owner = user.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value;

            await _context.GoalWorkout.AddAsync(gwModel);

            await _context.SaveChangesAsync();

            return gwModel.Id;
        }

        public async Task<bool> DeleteAsync(int id, ClaimsPrincipal user)
        { 
            var gwWithUser = await _context.GoalWorkout
                .Include(gw => gw.Goal)
                .ThenInclude(g => g.Profile)
                .ThenInclude(p => p.User)
                .SingleOrDefaultAsync(gw => gw.Id == id);
            
            if (gwWithUser == null)
            {
                return false;
            }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, gwWithUser.Goal.Profile.User, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Delete GoalWorkout
            _context.GoalWorkout.Remove(gwWithUser);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var goalWorkout = await _context.GoalWorkout
                                            .Include(gw => gw.Goal)
                                                .ThenInclude(g => g.Profile)
                                                .ThenInclude(p => p.User)
                                            .SingleOrDefaultAsync(gw => gw.Id == id);

            if (goalWorkout == null)
            {
                return false;
            }

            // Authorize admin and owner
            var authResult = await _auth.AuthorizeAsync(user, goalWorkout.Goal.Profile.User, "BigBrotherPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Make sure only one operation and that it's applied to IsCompleted OR DaysFromStart
            if (patch.Operations.Count().Equals(1) &&
               (patch.Operations[0].path == "/IsCompleted" ||
                patch.Operations[0].path == "/DaysFromStart"))
            {
                patch.ApplyTo(goalWorkout);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
