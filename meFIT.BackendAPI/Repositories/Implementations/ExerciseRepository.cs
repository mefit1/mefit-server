﻿using meFIT.BackendAPI.DataContext;
using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class ExerciseRepository : IExerciseRepository
    {
        private readonly AppDbContext _context;

        public ExerciseRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AddAsync(DTOs.Exercise.WithCategoriesPost exerciseDTO, ClaimsPrincipal user)
        {
            //Converting to Exercise and adding to database
            var exercise = new Exercise(exerciseDTO);

            if (exercise == null)
            {
                return false;
            }

            exercise.Owner = user.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Email).Value;

            var exerciseEntry = await _context.Exercise.AddAsync(exercise);

            if (exerciseEntry.Entity == null)
            {
                return false;
            }

            //Check if categories with id exists in the database, if it does make a new ExerciseCategory
            var categoryIds = exerciseDTO.TargetMuscleGroups;
            foreach (var id in categoryIds)
            {
                var category = await _context.Category.FirstOrDefaultAsync(cat => cat.Id == id);

                if (category != default(Category))
                {
                    await _context.ExerciseCategory.AddAsync(
                        new ExerciseCategory { Category = category, Exercise = exerciseEntry.Entity });
                }
            }

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            //Find exercise to delete
            var exercise = await _context.Exercise.FindAsync(id);
            if (exercise == null)
            {
                return false;
            }

            //Delete related exercise categories
            var exerciseCategories = exercise.TargetMuscleGroups;
            if (exerciseCategories != null)
            {
                _context.ExerciseCategory.RemoveRange(exerciseCategories);
            }

            //Delete related sets
            var sets = _context.Set.Where(s => s.ExerciseId == id);
            if (sets != null)
            {
                _context.Set.RemoveRange(sets);
            }

            //Delete exercise
            _context.Exercise.Remove(exercise);

            await _context.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Fetches all exercises including categories from DbContext,
        /// maps the <c>TargetMuscleGroups</c> to a list of strings based on
        /// category names and orders the exercises by the categories with 
        /// lowest alphabetical value.
        /// </summary>
        /// <returns>An ordered list of exercises including its category names</returns>
        public async Task<IEnumerable<DTOs.Exercise.WithCategories>> GetAllAsync()
        {
            var exercises = await _context.Exercise.Include(ex => ex.TargetMuscleGroups)
                                                        .ThenInclude(excat => excat.Category)
                                                   .Select(ex => new DTOs.Exercise.WithCategories(ex))
                                                   .ToListAsync();



            return exercises.OrderBy(ex => ex.Categories.OrderBy(tmg => tmg?.ConcreteName)
                                                        .FirstOrDefault()?.ConcreteName)
                            .ToList();
        }

        /// <summary>
        /// Fetches an exercise by <c>id</c> from DbContext and
        /// includes the category names.
        /// </summary>
        /// <param name="id">Id of exercise</param>
        /// <returns>An exercise including its category names</returns>
        public async Task<DTOs.Exercise.WithCategories> GetByIdAsync(int id)
        {
            var exercise = await _context.Exercise.Include(ex => ex.TargetMuscleGroups)
                                                    .ThenInclude(excat => excat.Category)
                                                  .FirstOrDefaultAsync(ex => ex.Id == id);

            var exerciseDTO = new DTOs.Exercise.WithCategories(exercise);

            if (exerciseDTO == null)
            {
                return null;
            }

            return exerciseDTO;
        }

        public async Task<DTOs.Exercise.Base> GetPostDTOByIdAsync(int id)
        {
            var entity = await _context.Exercise.FindAsync(id);
            
            if (entity == null)
            {
                return null;
            }

            return new DTOs.Exercise.Base(entity);
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument<Exercise> patch)
        {
            var exercise = await _context.Exercise.FindAsync(id);

            if (exercise == null)
            {
                return false;
            }

            patch.ApplyTo(exercise);
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
