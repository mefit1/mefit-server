﻿using meFIT.BackendAPI.DataContext;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories.Implementations
{
    public class GoalProgramWorkoutRepository : IGoalProgramWorkoutRepository
    {
        private readonly AppDbContext _context;
        private readonly IAuthorizationService _auth;

        public GoalProgramWorkoutRepository(AppDbContext context, IAuthorizationService auth)
        {
            _context = context;
            _auth = auth;
        }

        public async Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user)
        {
            var goalProgramWorkout = await _context.GoalProgramWorkout.FindAsync(id);

            if (goalProgramWorkout == null)
            {
                return false;
            }

            var userModel = await _context.User.SingleOrDefaultAsync(u => u.Email == goalProgramWorkout.Owner);
            var authResult = await _auth.AuthorizeAsync(user, userModel, "OwnerPolicy");
            if (!authResult.Succeeded) { throw new UnauthorizedAccessException(); }

            //Make sure only one operation and that it's applied to IsCompleted OR DaysFromStart
            if (patch.Operations.Count().Equals(1) &&
               (patch.Operations[0].path == "/IsCompleted" ||
                patch.Operations[0].path == "/DaysFromStart"))
            {
                patch.ApplyTo(goalProgramWorkout);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
