﻿using meFIT.BackendAPI.DTOs.Profile;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IProfileRepository
    {
        Task<DTOs.Profile.WithAllInformation> GetByIdAsync(int id, ClaimsPrincipal user);
        //Task<bool> AddAsync(DTOs.Profile.Post profile, ClaimsPrincipal user);
        Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user);
        Task<bool> DeleteAsync(int id, ClaimsPrincipal user);
    }
}
