﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IExerciseRepository
    {
        Task<bool> AddAsync(DTOs.Exercise.WithCategoriesPost exercise, ClaimsPrincipal user);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<DTOs.Exercise.WithCategories>> GetAllAsync();
        Task<DTOs.Exercise.WithCategories> GetByIdAsync(int id);
        Task<DTOs.Exercise.Base> GetPostDTOByIdAsync(int id);
        Task<bool> PatchAsync(int id, JsonPatchDocument<Exercise> patch);

    }
}
