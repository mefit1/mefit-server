﻿using meFIT.BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface ICategoryRepository
    {
        Task<ICollection<Category>> GetAllAsync();
    }
}
