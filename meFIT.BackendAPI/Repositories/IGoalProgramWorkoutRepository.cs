﻿using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IGoalProgramWorkoutRepository
    {
        Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user);
    }
}
