﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IUserRepository
    {
        Task<bool> RegisterAsync(DTOs.User.Register user, Func<string, string> redirect);
        Task<DTOs.User.Output> LoginAsync(DTOs.User.Login user);
        Task<DTOs.User.WithProfile> GetByIdAsync(int id, ClaimsPrincipal user);
        Task<bool> Delete(int id, ClaimsPrincipal user);
        Task<string> GetUserURL(ClaimsPrincipal user, string requestURL);
        Task<bool> PatchUser(int id, JsonPatchDocument patch, ClaimsPrincipal user);
        Task<bool> UpdatePassword(int id, DTOs.User.UpdatePassword password, ClaimsPrincipal user);
        Task<bool> UpdateRole(int id, DTOs.User.UpdateRole role, ClaimsPrincipal user);
        Task<IEnumerable<DTOs.User.WithRights>> GetAllAsync();
        Task<bool> ConfirmEmailAsync(string token);
        Task<bool> SendNewPasswordAsync(string email);
        Task<bool> PutUser(int id, DTOs.User.UpdateUser userDTO, ClaimsPrincipal user);
    }
}
