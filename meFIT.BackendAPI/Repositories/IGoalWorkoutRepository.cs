﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IGoalWorkoutRepository
    {
        Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user);
        Task<int> AddAsync(DTOs.GoalWorkout.Post workout, ClaimsPrincipal user);
        Task<bool> DeleteAsync(int id, ClaimsPrincipal user);
    }
}
