﻿using meFIT.BackendAPI.DTOs.Workout;
using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Repositories
{
    public interface IWorkoutRepository
    {
        Task<IEnumerable<IWithSets>> GetAllAsync();
        Task<WithFullExercise> GetByIdAsync(int id);
        Task<bool> AddAsync(Post workoutDTO, ClaimsPrincipal user);
        Task<bool> PatchAsync(int id, JsonPatchDocument patch, ClaimsPrincipal user);
        Task<bool> DeleteAsync(int id);
    }
}
