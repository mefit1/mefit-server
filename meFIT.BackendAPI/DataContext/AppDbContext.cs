﻿using meFIT.BackendAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.DataContext
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
        { }

        #region Entities
        public DbSet<Address> Address { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Exercise> Exercise { get; set; }
        public DbSet<ExerciseCategory> ExerciseCategory { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<Goal> Goal { get; set; }
        public DbSet<GoalProgram> GoalProgram { get; set; }
        public DbSet<GoalProgramWorkout> GoalProgramWorkout { get; set; }
        public DbSet<GoalProgramCategory> GoalProgramCategory { get; set; }
        public DbSet<GoalWorkout> GoalWorkout { get; set; }
        public DbSet<Profile> Profile { get; set; }
        public DbSet<Models.Program> Program { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkout { get; set; }
        public DbSet<Set> Set { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Workout> Workout { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            SeedData(builder);

            #region FluentAPI examples
            // one-to-one
            //builder.Entity<Student>()
            //    .HasOne<StudentAddress>(s => s.Address)
            //    .WithOne(ad => ad.Student)
            //    .HasForeignKey<StudentAddress>(ad => ad.AddressId);

            // one-to-many
            //builder.Entity<Student>()
            //    .HasOne<Grade>(s => s.Grade)
            //    .WithMany(g => g.Students)
            //    .HasForeignKey(s => s.GradeId);

            // many-to-many
            //builder.Entity<StudentCourse>().HasKey(sc => new { sc.StudentId, sc.CourseId });

            //builder.Entity<StudentCourse>()
            //    .HasOne<Student>(sc => sc.Student)
            //    .WithMany(s => s.StudentCourses)
            //    .HasForeignKey(sc => sc.StudentId);


            //builder.Entity<StudentCourse>()
            //    .HasOne<Course>(sc => sc.Course)
            //    .WithMany(s => s.StudentCourses)
            //    .HasForeignKey(sc => sc.CourseId);
            #endregion

            #region GoalWorkout
            builder.Entity<GoalWorkout>()
                .HasOne(navs => navs.Goal)
                .WithMany(navs => navs.GoalWorkouts)
                .HasForeignKey(props => props.GoalId);
            #endregion

            #region GoalProgramWorkout
            builder.Entity<GoalProgramWorkout>()
                .HasOne(navs => navs.GoalProgram)
                .WithMany(navs => navs.GoalProgramWorkouts)
                .HasForeignKey(props => props.GoalProgramId);
            #endregion

            #region ProgramWorkout
            builder.Entity<ProgramWorkout>()
                .HasOne(navs => navs.Program)
                .WithMany(navs => navs.ProgramWorkouts)
                .HasForeignKey(props => props.ProgramId);
            #endregion

            #region ExerciseCategory
            builder.Entity<ExerciseCategory>().HasKey(props =>
                new { props.ExerciseId, props.CategoryId }
            );
            #endregion

            #region ProgramCategory
            builder.Entity<ProgramCategory>().HasKey(props =>
                new { props.ProgramId, props.CategoryId }
            );
            #endregion

            #region GoalProgramCategory
            builder.Entity<GoalProgramCategory>().HasKey(props =>
                new { props.GoalProgramId, props.CategoryId }
            );
            #endregion

            #region WorkoutCategory
            builder.Entity<WorkoutCategory>().HasKey(props =>
                new { props.WorkoutId, props.CategoryId }
            );
            #endregion

            #region User
            builder.Entity<User>()
                .HasOne(navs => navs.Profile)
                .WithOne(navs => navs.User)
                .HasForeignKey<Profile>(props => props.UserId);
            #endregion
        }

        public static void SeedData(ModelBuilder builder)
        {
            builder.Entity<Category>().HasData(
                new Category { Id = 1, GeneralName = "Torso", ConcreteName = "Abs" },
                new Category { Id = 2, GeneralName = "Torso", ConcreteName = "Chest" },
                new Category { Id = 3, GeneralName = "Torso", ConcreteName = "Middle back" },
                new Category { Id = 4, GeneralName = "Legs", ConcreteName = "Legs" },
                new Category { Id = 5, GeneralName = "Legs", ConcreteName = "Calfs" },
                new Category { Id = 6, GeneralName = "Legs", ConcreteName = "Glutes" },
                new Category { Id = 7, GeneralName = "Arms", ConcreteName = "Shoulders" },
                new Category { Id = 8, GeneralName = "Torso", ConcreteName = "Lower back" },
                new Category { Id = 9, GeneralName = "Arms", ConcreteName = "Biceps" },
                new Category { Id = 10, GeneralName = "Arms", ConcreteName = "Triceps" },
                new Category { Id = 11, GeneralName = "Legs", ConcreteName = "Front legs" },
                new Category { Id = 12, GeneralName = "Torso", ConcreteName = "Sideabs" },
                new Category { Id = 13, GeneralName = "Legs", ConcreteName = "Quadriceps" },
                new Category { Id = 14, GeneralName = "Torso", ConcreteName = "Upper back" },
                new Category { Id = 15, GeneralName = "Legs", ConcreteName = "Hamstring" },
                new Category { Id = 16, GeneralName = "Arms", ConcreteName = "Forearms" }
            );
            builder.Entity<Exercise>().HasData(
                new Exercise
                {
                    Id = 1,
                    Name = "Skips",
                    Description = "Skip on one foot for 10 seconds, than repeat on the other foot.",
                    Image = "",
                    Video = "",
                    Owner = "harald.rex@kongehuset.no"
                },
                new Exercise
                {
                    Id = 2,
                    Name = "Sit-Ups",
                    Description = "Lay on your back, bring your arms behind your " +
                                  "head and lift your torso before you slowly lower it again.",
                    Image = "",
                    Video = "",
                    Owner = "harald.rex@kongehuset.no"
                },
                new Exercise
                {
                    Id = 3,
                    Name = "Push-Ups",
                    Description = "Lay faced down, bring your hands to the floor on both sides " +
                                  "next to your chest, lift your body so only your hands and toes touch the floor. " +
                                  "When your arms are straight, lower your body steady to the floor again.",
                    Image = "",
                    Video = "",
                    Owner = "harald.rex@kongehuset.no"
                }
            );
            builder.Entity<ExerciseCategory>().HasData(
                new ExerciseCategory { CategoryId = 1, ExerciseId = 3 },
                new ExerciseCategory { CategoryId = 2, ExerciseId = 3 },
                new ExerciseCategory { CategoryId = 1, ExerciseId = 2 },
                new ExerciseCategory { CategoryId = 15, ExerciseId = 1 },
                new ExerciseCategory { CategoryId = 11, ExerciseId = 1 },
                new ExerciseCategory { CategoryId = 8, ExerciseId = 1 },
                new ExerciseCategory { CategoryId = 6, ExerciseId = 1 },
                new ExerciseCategory { CategoryId = 5, ExerciseId = 1 },
                new ExerciseCategory { CategoryId = 4, ExerciseId = 1 }
            );
            builder.Entity<Workout>().HasData(
                new Workout
                {
                    Id = 1,
                    Name = "Basic Circle Workout",
                    Owner = "harald.rex@kongehuset.no"
                }
            );
            builder.Entity<WorkoutCategory>().HasData(
                new WorkoutCategory { WorkoutId = 1, CategoryId = 1 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 4 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 5 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 6 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 8 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 11 },
                new WorkoutCategory { WorkoutId = 1, CategoryId = 15 }
            );
            builder.Entity<Set>().HasData(
                new Set
                {
                    Id = 1,
                    ExerciseId = 1,
                    WorkoutId = 1,
                    ExerciseRepetitions = 10
                },
                new Set
                {
                    Id = 2,
                    ExerciseId = 2,
                    WorkoutId = 1,
                    ExerciseRepetitions = 10
                },
                new Set
                {
                    Id = 3,
                    ExerciseId = 1,
                    WorkoutId = 1,
                    ExerciseRepetitions = 10
                },
                new Set
                {
                    Id = 4,
                    ExerciseId = 2,
                    WorkoutId = 1,
                    ExerciseRepetitions = 10
                }
            );
            builder.Entity<Models.Program>().HasData(
                new Models.Program
                {
                    Id = 1,
                    Name = "Basic Core Program",
                    Owner = "harald.rex@kongehuset.no"
                }
            );
            builder.Entity<ProgramWorkout>().HasData(
                new ProgramWorkout { Id = 1, ProgramId = 1, WorkoutId = 1, DaysFromStart = 0 },
                new ProgramWorkout { Id = 2, ProgramId = 1, WorkoutId = 1, DaysFromStart = 2 }
            );
            builder.Entity<ProgramCategory>().HasData(
                new ProgramCategory { ProgramId = 1, CategoryId = 1 },
                new ProgramCategory { ProgramId = 1, CategoryId = 4 },
                new ProgramCategory { ProgramId = 1, CategoryId = 5 },
                new ProgramCategory { ProgramId = 1, CategoryId = 6 },
                new ProgramCategory { ProgramId = 1, CategoryId = 8 },
                new ProgramCategory { ProgramId = 1, CategoryId = 11 },
                new ProgramCategory { ProgramId = 1, CategoryId = 15 }
            );
            builder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Harald",
                    LastName = "Rex",
                    DOB = new DateTime(1937, 2, 21),
                    Email = "harald.rex@kongehuset.no",
                    Role = Helpers.Role.Admin,
                    IsConfirmed = true
                },
                new User
                {
                    Id = 2,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Charles",
                    LastName = "Barkley",
                    DOB = new DateTime(1963, 2, 20),
                    Email = "charles.barkley@nba.com",
                    Role = Helpers.Role.Contributer,
                    IsConfirmed = true
                },
                new User
                {
                    Id = 3,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Haakon Magnus",
                    LastName = "Crown Prince of Norway",
                    DOB = new DateTime(1973, 07, 20),
                    Email = "haakon.magnus@kongehuset.no",
                    Role = Helpers.Role.User,
                    IsConfirmed = true
                },
                new User
                {
                    Id = 4,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Jan",
                    LastName = "Johansen",
                    DOB = new DateTime(1962, 1, 19),
                    Email = "j_johansen@hotmail.com",
                    Role = Helpers.Role.User,
                    IsConfirmed = true
                },
                new User
                {
                    Id = 5,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Martin",
                    LastName = "Ødegaard",
                    DOB = new DateTime(2000, 12, 30),
                    Email = "martinmann@gmail.com",
                    Role = Helpers.Role.User,
                    IsConfirmed = true,
                    ApplyForContributor = true
                },
                new User
                {
                    Id = 6,
                    Password = new PasswordHasher<User>().HashPassword(new Models.User(), "jhhfkjgFJKOJFWJ8ujrikjil"),
                    FirstName = "Kari",
                    LastName = "Nordmann",
                    DOB = new DateTime(1955, 9, 20),
                    Email = "kari.nordmann@ciber.no",
                    Role = Helpers.Role.Contributer,
                    IsConfirmed = true
                }
            );
            builder.Entity<Address>().HasData(
                new Address { Id = 1, AddressLine1 = "Drammensveien 1", City = "Oslo", Country = "Norway", PostalCode = "0010" },
                new Address { Id = 2, AddressLine1 = "1st Avenue 45", City = "Leeds, AL", Country ="USA", PostalCode = "35094" },
                new Address { Id = 3, AddressLine1 = "Skaugumsåsen 1", City = "Asker", Country = "Norway", PostalCode = "1399" },
                new Address { Id = 4, AddressLine1 = "Lakkegata 14B", City = "Oslo", Country = "Norway", PostalCode = "0556" },
                new Address { Id = 5, AddressLine1 = "Gran Vía 100", City = "Madrid", Country = "Spain", PostalCode = "23487" },
                new Address { Id = 6, AddressLine1 = "Granskauen 1050F", City = "Hønefoss", Country = "Norway", PostalCode = "4510" }
            );
            builder.Entity<Gender>().HasData(
                new Gender { Id = 1, Name = "Male" },
                new Gender { Id = 2, Name = "Female" },
                new Gender { Id = 3, Name = "Other" }
            );
            builder.Entity<Models.Profile>().HasData(
                new Models.Profile
                {
                    Id = 1,
                    AddressId = 1,
                    GenderId = 1,
                    UserId = 1,
                    Disabilities = "Dysleksia, transplanted hip",
                    MedicalConditions = "Weak heart",
                    Weight = 94.5f,
                    Height = 187.0f,
                    Owner = "harald.rex@kongehuset.no"
                },
                new Models.Profile
                {
                    Id = 2,
                    AddressId = 2,
                    GenderId = 1,
                    UserId = 2,
                    Disabilities = "",
                    MedicalConditions = "",
                    Weight = 114.5f,
                    Height = 198.0f,
                    Owner = "charles.barkley@nba.com"
                },
                new Models.Profile
                {
                    Id = 3,
                    AddressId = 3,
                    GenderId = 1,
                    UserId = 3,
                    Disabilities = "",
                    MedicalConditions = "",
                    Weight = 100.5f,
                    Height = 193.0f,
                    Owner = "haakon.magnus@kongehuset.no"
                },
                new Models.Profile
                {
                    Id = 4,
                    AddressId = 4,
                    GenderId = 1,
                    UserId = 4,
                    Disabilities = "",
                    MedicalConditions = "",
                    Weight = 94.5f,
                    Height = 187.0f,
                    Owner = "j_johansen@hotmail.com"
                },
                new Models.Profile
                {
                    Id = 5,
                    AddressId = 5,
                    GenderId = 1,
                    UserId = 5,
                    Disabilities = "",
                    MedicalConditions = "",
                    Weight = 114.5f,
                    Height = 198.0f,
                    Owner = "martinmann@gmail.com"
                },
                new Models.Profile
                {
                    Id = 6,
                    AddressId = 6,
                    GenderId = 2,
                    UserId = 6,
                    Disabilities = "",
                    MedicalConditions = "",
                    Weight = 62f,
                    Height = 170f,
                    Owner = "kari.nordmann@ciber.no"
                }
            );

            builder.Entity<GoalProgram>().HasData(
                new GoalProgram { Id = 1, Name = "Basic Core Program" }
            );
            builder.Entity<GoalProgramWorkout>().HasData(
                new GoalProgramWorkout 
                { 
                    Id = 1, 
                    GoalProgramId = 1, 
                    DaysFromStart = 0, 
                    WorkoutId = 1, 
                    IsCompleted = false,
                    Owner = "harald.rex@kongehuset.no"
                },
                new GoalProgramWorkout
                {
                    Id = 2,
                    GoalProgramId = 1,
                    DaysFromStart = 2,
                    WorkoutId = 1,
                    IsCompleted = false,
                    Owner = "harald.rex@kongehuset.no"
                }
            );
            builder.Entity<GoalProgramCategory>().HasData(
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 1 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 4 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 5 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 6 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 8 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 11 },
                new GoalProgramCategory { GoalProgramId = 1, CategoryId = 15 }
            );
            builder.Entity<Goal>().HasData(
                new Goal
                {
                    Id = 1,
                    StartDate = new DateTime(2020, 12, 24),
                    IsActive = true,
                    IsCompleted = false,
                    ProfileId = 1,
                    GoalProgramId = 1,
                    Owner = "harald.rex@kongehuset.no"
                }
            );
            builder.Entity<GoalWorkout>().HasData(
                new GoalWorkout 
                { 
                    Id = 1, 
                    GoalId = 1, 
                    WorkoutId = 1, 
                    DaysFromStart = 5, 
                    IsCompleted = false,
                    Owner = "harald.rex@kongehuset.no"
                }
            );
        }
    }
}
