﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Helpers
{
    public static class Validate
    {
        public static bool Password(string password)
        {
            // Must be min 8 characters
            if (password.Count() < 8)
                return false;

            // Must contain lower case characters
            if (!password.Any(p => Char.IsLower(p)))
                return false;

            // Must contain upper case characters
            if (!password.Any(p => Char.IsUpper(p)))
                return false;

            // Must contain digits
            if (!password.Any(p => Char.IsDigit(p)))
                return false;

            // Must contain special characters
            if (!password.Any(p => !Char.IsLetterOrDigit(p)))
                return false;

            return true;
        }
    }
}
