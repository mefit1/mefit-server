﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Helpers
{
    public static class PasswordGenerator
    {
        const string CHAR_SET = @"abcdefghijklmnopqursuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789!@£$%^&*()#€";
        const int PASSWORD_SIZE = 16;

        public static string GenerateRandom()
        {
            var random = new Random();
            char[] password = new char[PASSWORD_SIZE];

            do
            {
                for (int i = 0; i < PASSWORD_SIZE; i++)
                {
                    password[i] = CHAR_SET[random.Next(CHAR_SET.Length - 1)];
                }
            } while (!Validate.Password(String.Join(null, password)));

            return String.Join(null, password);
        }
    }
}
