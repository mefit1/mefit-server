﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace meFIT.BackendAPI.Helpers
{
    public static class Role
    {
        public const string Admin = "Admin";
        public const string Contributer = "Contributer, Admin";
        public const string User = "User, Contributer, Admin";
    }
}
